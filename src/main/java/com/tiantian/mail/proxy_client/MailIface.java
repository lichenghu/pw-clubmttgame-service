package com.tiantian.mail.proxy_client;

import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.mail.settings.MailConfig;
import com.tiantian.mail.thrift.mail.MailService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class MailIface extends IFace<MailService.Iface> {


    private static class MailIfaceHolder {
        private final static MailIface instance = new MailIface();
    }

    private MailIface() {
    }

    public static MailIface instance() {
        return MailIfaceHolder.instance;
    }

    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new MailService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new com.tiantian.framework.thrift.client.ClientPool(MailConfig.getInstance().getHost(),
                MailConfig.getInstance().getPort());
    }

    @Override
    protected Class<MailService.Iface> faceClass() {
        return MailService.Iface.class;
    }
}
