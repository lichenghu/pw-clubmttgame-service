package com.tiantian.clubmttgame.manager.texas;

import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 *
 */
public class PokerManager {
    /**
     * 随机产生牌
     * @return
     */
    public static List<Poker> initPokers() {
        List<Poker> pokerList = new ArrayList<>();
        for (int i = 2; i <= 53; i++) {
             Poker poker = Poker.create(i);
             pokerList.add(poker);
        }
//        Collections.shuffle(pokerList);
//        Collections.shuffle(pokerList);
//        Collections.shuffle(pokerList);
//        Collections.shuffle(pokerList);
//        Collections.shuffle(pokerList);
        for(int a = 0; a < 256 ; a++) {
            Random r = new Random();
            int rint1 = r.nextInt(52);
            int rint2 = r.nextInt(52);
            Collections.swap(pokerList, rint1, rint2);
        }
        return pokerList;
    }

    public static List<Map.Entry<String, PokerOuts>> getWinnerPokerOutsList( Map<String, String> userCards,  List<String> deskCards) {
        Set<Map.Entry<String, String>> mapSet = userCards.entrySet();
        Map<String, PokerOuts> outsMap = new HashMap<>();
        for (Map.Entry<String, String> entry : mapSet) {
             String userId = entry.getKey();
             String[] cards = entry.getValue().split(",");
             List<Poker> pokerList = new ArrayList<>();
             Poker poker1 = Poker.create(cards[0]);
             pokerList.add(poker1);
             Poker poker2 = Poker.create(cards[1]);
             pokerList.add(poker2);
             if (deskCards != null && deskCards.size() > 0) {
                 for (String card : deskCards) {
                      Poker poker = Poker.create(card);
                      pokerList.add(poker);
                 }
             }
             PokerOuts pokerOuts = loadPokerOuts(pokerList);
             outsMap.put(userId, pokerOuts);
        }
        // 排序
        List<Map.Entry<String, PokerOuts>> list = new ArrayList<>(outsMap.entrySet());
        // 倒序
        Collections.sort(list, new Comparator<Map.Entry<String, PokerOuts>>() {
            @Override
            public int compare(Map.Entry<String, PokerOuts> o1, Map.Entry<String, PokerOuts> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        Map.Entry<String, PokerOuts> tmpEntry = null;
        List<Map.Entry<String, PokerOuts>> returnList = new ArrayList<>();
        boolean isOver = false;
        while (list.size() > 0 && !isOver) {
            Map.Entry<String, PokerOuts> entry = list.remove(0);
            if (tmpEntry != null) {
                if (entry.getValue().compareTo(tmpEntry.getValue()) == 0) {
                    returnList.add(entry);
                    tmpEntry = entry;
                } else {
                    isOver = true;
                }
            } else {
                returnList.add(entry);
                tmpEntry = entry;
            }
        }
        return returnList;
    }

    public static Map<String, PokerOuts> getUsersPokerOutsList(Map<String, String> userCards, List<String> deskCards) {
        Set<Map.Entry<String, String>> mapSet = userCards.entrySet();
        Map<String, PokerOuts> outsMap = new HashMap<>();
        for (Map.Entry<String, String> entry : mapSet) {
            String sitNum = entry.getKey();
            String[] cards = entry.getValue().split(",");
            List<Poker> pokerList = new ArrayList<>();
            Poker poker1 = Poker.create(cards[0]);
            pokerList.add(poker1);
            Poker poker2 = Poker.create(cards[1]);
            pokerList.add(poker2);
            if (deskCards != null && deskCards.size() > 0) {
                for (String card : deskCards) {
                    Poker poker = Poker.create(card);
                    pokerList.add(poker);
                }
            }
            PokerOuts pokerOuts = loadPokerOuts(pokerList);
            outsMap.put(sitNum, pokerOuts);
        }
        return outsMap;
    }

    public static PokerOuts getPokerOuts(String cardStr, List<String> deskCards) {
        if (StringUtils.isBlank(cardStr)) {
            return null;
        }
        String[] cards = cardStr.split(",");
        List<Poker> pokerList = new ArrayList<>();
        Poker poker1 = Poker.create(cards[0]);
        pokerList.add(poker1);
        Poker poker2 = Poker.create(cards[1]);
        pokerList.add(poker2);
        if (deskCards != null && deskCards.size() > 0) {
            for (String card : deskCards) {
                Poker poker = Poker.create(card);
                pokerList.add(poker);
            }
        }
        return loadPokerOuts(pokerList);
    }

    private static PokerOuts loadPokerOuts(List<Poker> pokerList) {
        PokerRecognizer recognizer = new PokerRecognizer();
        recognizer.load(pokerList);
        PokerOuts outs = recognizer.getMaxOuts();
        return outs;
    }
}
