package com.tiantian.clubmttgame.manager.model;

import com.alibaba.fastjson.JSON;
import com.tiantian.clubmttgame.data.redis.RedisUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;


/**
 *
 */
public abstract class ModeBase {
    private static Logger LOG = LoggerFactory.getLogger(ModeBase.class);

    @FieldIgnore
    protected Map<String, Object> filedValMap = new HashMap<>();
    private static final Object lock = new Object();

    public ModeBase()
    {
        if (fieldMap() == null || fieldMap().size() == 0) {
            loadFileMap();
        }
    }

    protected abstract boolean hasLoad();

    protected void loadFileMap() {
        synchronized (lock) {
            if (fieldMap() != null && fieldMap().size() > 0) {
                return;
            }
            if (fieldMap() != null && fieldMap().size() == 0) {
                Field[] fields = this.getClass().getDeclaredFields();
                for (Field field : fields) {
                    field.setAccessible(true);
                    FieldIgnore annotation = field.getAnnotation(FieldIgnore.class);
                    if (annotation == null) {
                        fieldMap().put(field.getName(), field);
                    }
                }
            }
        }
    }
    public abstract Map<String, Field> fieldMap();

    public boolean loadModel() {
        Map<String, String> map = RedisUtil.getMap(key());
        if (map == null || map.size() == 0) {
            return false;
        }
        Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
        filedValMap.clear();
        for (Map.Entry<String, String> entry : mapEntrySet) {
             String key = entry.getKey();
             String value = entry.getValue();
             Field field =  fieldMap().get(key);
             if (field != null) {
                 try {
                    field.set(this, value);
                    filedValMap.put(field.getName(), field.get(this));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
             }
        }
        return true;
    }

    public void saveWithNotScript() {
        List<String[]> list = new ArrayList<>();
        Map<String, Object> newFiledValMap = new HashMap<>();
        for (Map.Entry<String, Field> fieldEntry : fieldMap().entrySet()) {
            try {
                String name = fieldEntry.getKey();
                Field field = fieldEntry.getValue();
                Object obj = field.get(this);
                newFiledValMap.put(name, obj);
                if (obj == null) {
                    String[] strings = new String[]{"hdel", key(), name};
                    list.add(strings);
                } else {
                    Object oldObj = filedValMap.get(field.getName());
                    if (!obj.equals(oldObj)) {
                        String[] strings = new String[]{"hset", key(), name, String.valueOf(obj)};
                        list.add(strings);
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        if (list.size() > 0) {
            RedisUtil.pipMapCmd(list);
        }
        filedValMap.clear();
        filedValMap = null;
        filedValMap = newFiledValMap;
    }

    public boolean save() {
        if (StringUtils.isBlank(getSaveScriptSha())) {
            saveWithNotScript();
            return true;
        }
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        keys.add(key());
        values.add("");
        keys.add("version");
        values.add(StringUtils.isBlank(getVersion()) ? "1" : getVersion());
        Map<String, Object> newFiledValMap = new HashMap<>();
        for (Map.Entry<String, Field> fieldEntry : fieldMap().entrySet()) {
            try {
                String name = fieldEntry.getKey();
                Field field = fieldEntry.getValue();
                Object obj = field.get(this);
                newFiledValMap.put(name, obj);
                if (obj == null) {
                    keys.add(name);
                    values.add("nil");
                } else {
                    Object oldObj = filedValMap.get(field.getName());
                    if (!obj.equals(oldObj)) {
                        keys.add(name);
                        values.add(String.valueOf(obj));
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        filedValMap.clear();
        filedValMap = null;
        filedValMap = newFiledValMap;
        String result =  (String) RedisUtil.execScript(getSaveScriptSha(), keys, values);
        return  "1".equalsIgnoreCase(result);
    }

    public abstract String key();

    public String getSaveScriptSha() {
        return null;
    }

    public String getVersion() {
        return null;
    }
}
