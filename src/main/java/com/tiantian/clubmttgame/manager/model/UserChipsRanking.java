package com.tiantian.clubmttgame.manager.model;

/**
 *
 */
public class UserChipsRanking {
    private String userId;
    private Long leftChips;
    private Long beginChips;
    private Integer allinIndex;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getLeftChips() {
        return leftChips;
    }

    public void setLeftChips(Long leftChips) {
        this.leftChips = leftChips;
    }

    public Long getBeginChips() {
        return beginChips;
    }

    public void setBeginChips(Long beginChips) {
        this.beginChips = beginChips;
    }

    public Integer getAllinIndex() {
        return allinIndex;
    }

    public void setAllinIndex(Integer allinIndex) {
        this.allinIndex = allinIndex;
    }
}
