package com.tiantian.clubmttgame.manager.model;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
public class RankingList {
    private Map<String, RankingInfo> rankingMap = new HashMap<>();
    private AtomicInteger orderSeq = new AtomicInteger(0);

    public void add(String userId, long chips) {
        RankingInfo rankingInfo = rankingMap.remove(userId);
        if (rankingInfo == null) {
            rankingInfo = new RankingInfo(userId, chips, orderSeq.addAndGet(1));
        } else {
            rankingInfo.setChips(chips);
            rankingInfo.setOrder(orderSeq.addAndGet(1));
        }
        rankingMap.put(userId, rankingInfo);
    }

    public List<RankingInfo> sortRankings() {
        List<RankingInfo> list = new ArrayList(rankingMap.values());
        Collections.sort(list, (o1, o2) -> o1.compareTo(o2));
        int i = 0;
        long lastChips = -1;
        for (RankingInfo rankingInfo : list) {
             if (rankingInfo.getChips() == 0 || (rankingInfo.getChips() > 0 && rankingInfo.getChips() != lastChips)) {
                 i++;
             }
             lastChips = rankingInfo.getChips();
             rankingInfo.setRanking(i);
        }
        return list;
    }

    public static class RankingInfo implements Comparable<RankingInfo>{
        private String userId;
        private long chips;
        private int order;
        private int ranking;

        public RankingInfo() {
        }

        public RankingInfo(String userId, long chips, int order) {
            this.userId = userId;
            this.chips = chips;
            this.order = order;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public long getChips() {
            return chips;
        }

        public void setChips(long chips) {
            this.chips = chips;
        }

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public String toString() {
            return userId + ":" + chips + ":" + ranking;
        }

        public int getRanking() {
            return ranking;
        }

        public void setRanking(int ranking) {
            this.ranking = ranking;
        }

        @Override
        public int compareTo(RankingInfo o) {
            long result = o.chips - this.chips;
            if (result > 0) {
                return 1;
            } else if (result < 0) {
                return -1;
            } else {
                return o.getOrder() - this.getOrder();
            }
        }
    }
}
