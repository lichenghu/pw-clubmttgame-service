package com.tiantian.clubmttgame.manager.model;

import com.tiantian.clubmttgame.data.redis.RedisUtil;
import com.tiantian.clubmttgame.manager.constants.GameConstants;

/**
 *
 */
public class RoomUsers {
    public static void addOnlineUsers(String roomId, String userId) {
       boolean isMem = RedisUtil.sismember(GameConstants.SPINGO_ROOM_ONLINE_USERS_KEY + roomId, userId);
       if (!isMem) {
           RedisUtil.sadd(GameConstants.SPINGO_ROOM_ONLINE_USERS_KEY + roomId, userId);
       }
    }

    public static void removeOnlineUsers(String roomId, String userId) {
        RedisUtil.srem(GameConstants.SPINGO_ROOM_ONLINE_USERS_KEY + roomId, userId);
    }
}
