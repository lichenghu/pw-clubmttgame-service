package com.tiantian.clubmttgame.manager.model;

/**
 *
 */
public class UserBetLog {
    private String userId;
    private String sitNum;
    private long roundChips;
    private long totalChips;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getRoundChips() {
        return roundChips;
    }

    public void setRoundChips(long roundChips) {
        this.roundChips = roundChips;
    }

    public long getTotalChips() {
        return totalChips;
    }

    public void setTotalChips(long totalChips) {
        this.totalChips = totalChips;
    }

    public String getSitNum() {
        return sitNum;
    }

    public void setSitNum(String sitNum) {
        this.sitNum = sitNum;
    }
}
