package com.tiantian.clubmttgame.manager.model;

import com.tiantian.clubmttgame.data.redis.RedisUtil;
import com.tiantian.clubmttgame.manager.constants.GameConstants;
import org.apache.commons.lang.StringUtils;
import java.util.*;

/**
 *
 */
public class TableAllUser {

    private String tableId;
    // 座位号和userId
    private Map<String, String> gamingSitUserMap = new HashMap<>(); // 所有状态是gaming的玩家
    private Map<String, String> joinTableUserMap = new HashMap<>(); // 所有没有退出挂机的所有玩家
    private Map<String, String> gamingAndExitUserMap = new HashMap<>(); // 所有状态是gaming和挂机的玩家
    private Map<String, String> sitDownUserMap = new HashMap<>(); // 所有坐在座位上的玩家 sitNum - userId
    private Map<String, String> oldSitDownUserMap = new HashMap<>();// 不是最新坐下的玩家
    private Map<String, String> gamingAndExitAndHangUserMap = new HashMap<>(); // 所有状态是gaming和退出和挂机的玩家
    private Map<String, String> exitAndHangUserMap = new HashMap<>();
    private Map<String, String> exitUserMap = new HashMap<>();
    private Map<String, String> gamingAndHangMap = new HashMap<>();
    // 桌子里面所有的玩家ID
    private Collection<String> onlineTableUserIds = new HashSet<>();
    private Set<String> allSitNums;

    private TableAllUser() {
    }

    public static TableAllUser load(String tableId) {
        TableAllUser tableAllUser = new TableAllUser();
        tableAllUser.tableId = tableId;
        Map<String, String> tableMap = RedisUtil.getMap(GameConstants.SPINGO_TABLE_USER_KEY + tableId);
        tableAllUser.allSitNums = new HashSet<>();
        if (tableMap != null) {
            // 筛选出在gaming状态的游戏玩家
            Map<String, String> sitUserMap = new HashMap<>();
            Map<String, String> sitAndExitUserMap = new HashMap<>();
            Map<String, String> sitDownUsers = new HashMap<>();
            Map<String, String> oldSitDownUsers = new HashMap<>();
            Map<String, String> gamingHangExitUsers = new HashMap<>();
            Map<String, String> exitAndHangUserMap = new HashMap<>();
            Map<String, String> exitUserMap = new HashMap<>();
            Map<String, String> gamingAndHangMap = new HashMap<>();
            Set<String> exitSet = new HashSet<>();
            tableAllUser.allSitNums = tableMap.keySet();
            for (Map.Entry<String, String> entry : tableMap.entrySet()) {
                 String sitNum = entry.getKey();
                 String userId = entry.getValue();
                 sitDownUsers.put(sitNum, userId);

                 TableUser tableUser = TableUser.load(userId, tableId);
                 if (tableUser != null && !tableUser.isNull() && tableId.equalsIgnoreCase(tableUser.getTableId())) {
                     if(tableUser.isGaming() || tableUser.isExit() || tableUser.isHang()) {
                        gamingHangExitUsers.put(sitNum, userId);
                     }
                     if (tableUser.isExit() || tableUser.isHang()) {
                         exitAndHangUserMap.put(sitNum, userId);
                     }
                     if (tableUser.isGaming() || tableUser.isHang()) {
                         gamingAndHangMap.put(sitNum, userId);
                     }
                     if (!tableUser.isNewJoin()) {
                          oldSitDownUsers.put(sitNum, userId);
                     }
                     if (tableUser.isGaming()) {
                         sitUserMap.put(sitNum, userId);
                         sitAndExitUserMap.put(sitNum, userId);
                     }

                     if (tableUser.isExit()) { //退出游戏后挂机
                         sitAndExitUserMap.put(sitNum, userId);
                         exitUserMap.put(sitNum, userId);
                         exitSet.add(sitNum);
                     }
                 }
            }

            for (String exiSit : exitSet) { //删除挂机的
                 tableMap.remove(exiSit);
            }
            tableAllUser.gamingSitUserMap = sitUserMap;
            tableAllUser.joinTableUserMap = tableMap;
            tableAllUser.gamingAndExitUserMap = sitAndExitUserMap;
            tableAllUser.sitDownUserMap = sitDownUsers;
            tableAllUser.oldSitDownUserMap = oldSitDownUsers;
            tableAllUser.gamingAndExitAndHangUserMap = gamingHangExitUsers;
            tableAllUser.exitAndHangUserMap = exitAndHangUserMap;
            tableAllUser.exitUserMap = exitUserMap;
            tableAllUser.gamingAndHangMap = gamingAndHangMap;
            Map<String, String> tableAllUserMap = RedisUtil.getMap(GameConstants.SPINGO_TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                tableAllUser.onlineTableUserIds = tableAllUserMap.keySet();
            }
        }
        return tableAllUser;
    }

    // 加入玩家
    public void joinUser(String userId, String sitNum, boolean needJoinOnline) {
        RedisUtil.setMap(GameConstants.SPINGO_TABLE_USER_KEY + tableId, sitNum, userId);
        if (needJoinOnline) {
            joinOnline(userId);
        }
    }

    public void deleteUser(String sitNum) {
        if (StringUtils.isBlank(sitNum)) {
            return;
        }
        String userId = joinTableUserMap.get(sitNum);
        RedisUtil.delMapField(GameConstants.SPINGO_TABLE_USER_KEY + tableId, sitNum);
        if (StringUtils.isNotBlank(userId)) {
            deleteOnline(userId);
        }
    }

    public void deleteUserByUserId(String userId) {
        if (joinTableUserMap != null) {
            for (Map.Entry<String, String> entry : joinTableUserMap.entrySet()) {
                 String sitNum = entry.getKey();
                 String uId = entry.getValue();
                 if (uId.equalsIgnoreCase(userId)) {
                     deleteUser(sitNum);
                     break;
                }
            }
        }
        deleteOnline(userId);
    }

    //删除掉已经离开的玩家
    public void flushGamingSitUser(List<String> removeSitList) {
        for (String sitNum : removeSitList) {
             RedisUtil.delMapField(GameConstants.SPINGO_TABLE_USER_KEY + tableId, sitNum);
        }
    }

    public boolean isOnline(String userId) {
        return onlineTableUserIds.contains(userId);
    }

    // 根据玩家ID判断玩家是否在游戏中，并返回座位号，返回值为空则表示不在
    public String checkUserGaming(String userId) {
        if(gamingSitUserMap == null) {
           return null;
        }
        Set<Map.Entry<String, String>> entrySet = gamingSitUserMap.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
             String sitNum = entry.getKey();
             String uId = entry.getValue();
             if (uId.equalsIgnoreCase(userId)) {
                 return sitNum;
             }
        }
        return null;
    }

    public boolean isGamingOrHang(String userId) {
        return gamingAndHangMap.containsValue(userId);
    }

    public void userStandUp(String userId) {
        if (joinTableUserMap != null) {
            for (Map.Entry<String, String> entry : joinTableUserMap.entrySet()) {
                 String sitNum = entry.getKey();
                 String uId = entry.getValue();
                 if (uId.equalsIgnoreCase(userId)) {
                    if (StringUtils.isBlank(sitNum)) {
                        return;
                    }
                    RedisUtil.delMapField(GameConstants.SPINGO_TABLE_USER_KEY + tableId, sitNum);
                    break;
                 }
            }
        }
    }

    public Collection<String> getOnlineTableUserIds() {
        return onlineTableUserIds;
    }

    public void delSelf() {
        RedisUtil.del(GameConstants.SPINGO_TABLE_USER_KEY + tableId);
        RedisUtil.del(GameConstants.SPINGO_TABLE_USER_ONLINE_KEY + tableId);
    }

    // 加入在线
    public void joinOnline(String userId) {
        RedisUtil.setMap(GameConstants.SPINGO_TABLE_USER_ONLINE_KEY + tableId, userId, "");
    }

    // 删除在线
    public void deleteOnline(String userId) {
        RedisUtil.delMapField(GameConstants.SPINGO_TABLE_USER_ONLINE_KEY + tableId, userId);
    }

    // 玩家状态是游戏中或者是退出并且还坐在座位上的(需要给玩家发手牌)
    public Map<String, String> userGamingOrExitSitUserId() {
        return gamingAndExitUserMap;
    }

    public Map<String, String> userGamingExitHangSitUserId() {
        return gamingAndExitAndHangUserMap;
    }

    public boolean isExitOrHang(String sitNum) {
        if (StringUtils.isBlank(sitNum)) {
            return false;
        }
        return exitAndHangUserMap.containsKey(sitNum);
    }

    public boolean isExitOrHangByUserId(String userId) {
        if (StringUtils.isBlank(userId)) {
            return false;
        }
        return exitAndHangUserMap.containsValue(userId);
    }

    public boolean isExit(String sitNum) {
        if (sitNum == null) {
            return false;
        }
        return exitUserMap.containsKey(sitNum);
    }
    public boolean isExitByUserId(String userId) {
        if (userId == null) {
            return false;
        }
        return exitUserMap.containsValue(userId);
    }

    // 坐在座位上 包括 游戏中, 退出的, 等待下局开始中, 等待买入
    public Map<String, String> userSitDownUsers() {
        return sitDownUserMap;
    }

    public Map<String, String> getGamingSitUserMap() {
        return gamingSitUserMap;
    }

    public void setGamingSitUserMap(Map<String, String> gamingSitUserMap) {
        this.gamingSitUserMap = gamingSitUserMap;
    }

    public Map<String, String> getJoinTableUserMap() {
        return joinTableUserMap;
    }

    public void setJoinTableUserMap(Map<String, String> joinTableUserMap) {
        this.joinTableUserMap = joinTableUserMap;
    }

    public Map<String, String> getGamingAndExitUserMap() {
        return gamingAndExitUserMap;
    }

    public void setGamingAndExitUserMap(Map<String, String> gamingAndExitUserMap) {
        this.gamingAndExitUserMap = gamingAndExitUserMap;
    }

    public void setOnlineTableUserIds(Collection<String> onlineTableUserIds) {
        this.onlineTableUserIds = onlineTableUserIds;
    }

    public Set<String> getAllSitNums() {
        return allSitNums;
    }

    public Map<String, String> getOldSitDownUserMap() {
        return oldSitDownUserMap;
    }

    public void setOldSitDownUserMap(Map<String, String> oldSitDownUserMap) {
        this.oldSitDownUserMap = oldSitDownUserMap;
    }

    public Map<String, String> getExitUserMap() {
        return exitUserMap;
    }

    public void setExitUserMap(Map<String, String> exitUserMap) {
        this.exitUserMap = exitUserMap;
    }
}
