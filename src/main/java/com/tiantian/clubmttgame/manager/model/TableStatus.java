package com.tiantian.clubmttgame.manager.model;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.tiantian.clubmtt.akka.event.game.GameStartEvent;
import com.tiantian.clubmttgame.data.redis.RedisUtil;
import com.tiantian.clubmttgame.manager.constants.GameConstants;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.constants.GameStatus;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 */
public class TableStatus extends ModeBase {
    @FieldIgnore
    static Logger LOG = LoggerFactory.getLogger(TableStatus.class);
    @FieldIgnore
    private static Map<String, Field> filedMap =  new ConcurrentHashMap<>();
    @FieldIgnore
    private static AtomicBoolean LOAD_FLAG = new AtomicBoolean(false);
    @FieldIgnore
    private static final String SCRIPT_PATH = "redisscripts/table_status_save.lua";
    @FieldIgnore
    private static String SCRIPT_SHA = "";
    @FieldIgnore
    private String tableId;
    private String tableIndex;
    private String roomId;
    private String roomName;
    private String version;
    private String started; // 1 是
    private String startMills; // 开始时间
    // 玩家状态任务ID
    private String userOnlineTaskId;
    // 每局ID
    private String inningId;
    // 一个买入该买入不能清除
    private String buyIn;
    // 庄家座位号
    private String button;
    // 小盲注 座位号
    private String smallBlindNum;
    // 大盲注 座位号
    private String bigBlindNum;
    //小盲注
    private String smallBlindMoney;
    // 大盲注
    private String bigBlindMoney;
    // 游戏状态
    private String status;
    // 玩家下注状态 {sitNum:status}
    private String sitBetStatus;
    // 玩家上一次下注状态，分池的时候需要 {sitNum:status}
    private String lastSitBetStatus;
    // 当前需要下注人座位号
    private String currentBet;
    // 当前需要下注人座位号的开始时间戳
    private String currentBetTimes;
    // 当前需要下注人最大的人座位号(玩家加注的时候会改变)
    private String maxBetSitNum;
    // 当前需要下注人最大的人座位号所下的筹码(玩家加注的时候会改变)
    private String maxBetSitNumChips;
    // 当前需要下注人最大的人座位号所下增加的筹码
    private String maxBetSitNumAddChips;

    // 玩家当轮下注的记录格式: key(座位号) : value ({userId :xxx ,roundChips:100, totalChips:1000})
    private String usersBetsLog;
    // 玩家下注统计日志 key (座位号) : value 字符串逗号分隔(1.0,2.5,0.5)
    private String userBetRaiseLog;
    // 牌
    private String cards;
    // 玩家的底牌
    private String userCards;
    // 桌面的牌
    private String deskCards;
    // 下注池, key :sitNum , value : bet 的list
    private String betPool;
    // 上一次下注池不持久化到redis
    @FieldIgnore
    private String lastBetPool;
    // 上次更新的操作时间戳
    private String lastUpdateTimes;
    // 更新计数
    private String indexCount;

    private String needDelay;
    // 玩家的 座位号和userId 的map集合 key : sitNum, value userId
    private String sitUserIds;
    private String pwd;
    // 每回合的第一个下注的筹码
    private String firstBetChips = "0"; // 默认值为0
    // 上一次升盲注的时间
    private String nextUpBlindTimes;
    private String currentBlindLevel;// 当前盲注等级
    private String minUsers; // 桌子的开桌人数
    private String maxUsers;
    private String gameInfo;
    private String ante;
    private String leftUsers;
    private String averageChips;
    private String maxCanRebuyBlindLvl;
    private String reBuyInCnt; // 可以重复买入的最大次数
    private String rebuyUserIds; // 等待玩家买入的userId
    private String rebuyStartMills;// 开始买入时候的时间
    private String overList; // 结束游戏的玩家ID list
    private String userLeftChips;//每局结束时候玩家剩余筹码
    private String anteMap; // sitNum 和 ante
    private String totalUsers; // 当前总在线人数
    private String startRestTimeMills; // 开始休息时间
    private String restMills; // 休息时长
    private String userAllinSeq; //userId list
    private String userStartLeftMoney; // 游戏开始的时候玩家的剩余筹码, userId - chips 的 map


    public static TableStatus load(String tableId) {
        TableStatus tableStatus = new TableStatus();
        tableStatus.tableId = tableId;
        boolean result = tableStatus.loadModel();
        if (!result) {
            return null;
        }
        return tableStatus;
    }

    public static TableStatus init(String tableId) {
        TableStatus tableStatus = new TableStatus();
        tableStatus.tableId = tableId;
        return tableStatus;
    }

    public static TableStatus newNull() {
        return new TableStatus();
    }

    protected boolean hasLoad(){
        return LOAD_FLAG.compareAndSet(false, true);
    }
    public boolean save() {
        if (tableId == null) {
            throw new RuntimeException("tableId can not be null");
        }
        setLastUpdateTimes(System.currentTimeMillis() + "");
        if (StringUtils.isBlank(indexCount)) {
            indexCount = "1";
        }
        setIndexCount((Integer.parseInt(indexCount) + 1) + "");
        return super.save();
    }

    public boolean saveNotAddCnt() {
        if (tableId == null) {
            throw new RuntimeException("tableId can not be null");
        }
        setLastUpdateTimes(System.currentTimeMillis() + "");
        return super.save();
    }

    @Override
    public String key() {
        return GameConstants.SPINGO_TABLE_STATUS_KEY + tableId;
    }

    private TableStatus() {
        super();
    }

    public Map<String, Field> fieldMap(){
        return filedMap;
    }

    public boolean clearNotFlush() {
        setVersion("1");
        // 设置牌局ID
        setInningId("");
        setSmallBlindNum("");
        setBigBlindNum("");
        // 设置游戏状态为 大小盲注
        setStatus(GameStatus.READY.name());
        setSitBetStatus("");
        setLastSitBetStatus("");
        // 设置当前需要下注人座位号
        setCurrentBet("");
        // 当前需要下注人座位号的开始时间戳
        setCurrentBetTimes("");
        // 设置当前需要下注人最大的人座位号(玩家加注的时候会改变)
        setMaxBetSitNum("");
        // 设置最大下注
        setMaxBetSitNumChips("");

        setBetPool("");

        setUsersBetsLog("");
        // 清空上局的牌
        setCards("");
        // 清空上局玩家的底牌
        setUserCards("");
        // 清空上局桌面的牌
        setDeskCards("");

        setIndexCount("1");
        setNeedDelay("");
        setSitUserIds("");
        setPwd("");
        setFirstBetChips("0");
        setMaxBetSitNumAddChips("0");
        setUserBetRaiseLog("");

        setRebuyUserIds("");
        setRebuyStartMills("");
        setOverList("");
        setUserLeftChips("");
        setAnteMap("");
        setUserAllinSeq("");
        setUserStartLeftMoney("");
        // 保存到redis
        return true;
//        return save();
    }

    public void addPlayingSits(Collection<String> allSits) {
        if (allSits != null && allSits.size() > 0) {
            Map<String, String> map = new HashMap<>();
            for(String sit : allSits) {
                map.put(sit, "");
            }
            sitBetStatus = JSON.toJSONString(map);
        }

    }

    public void addPlayingSitsAndId(Map<String, String> sitAndUserIdMap) {
        if (sitAndUserIdMap != null) {
            sitUserIds = JSON.toJSONString(sitAndUserIdMap);
        } else {
            sitUserIds = "";
        }
    }

    // 获取可以下注的玩家集合，如果集合的大小为0则表示只有allin和弃牌
    public Set<String> canBetSits() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<String> set = new HashSet<>();
            Set<Map.Entry<String, String>> entries = map.entrySet();
            // 没有弃牌和allin
            for (Map.Entry<String, String> entry : entries) {
                if (!GameEventType.FOLD.equalsIgnoreCase(entry.getValue()) &&
                        !GameEventType.ALLIN.equalsIgnoreCase(entry.getValue())) {
                    set.add(entry.getKey());
                }
            }
            return set;
        }
        return null;
    }

    public void setSitBetStatusNotFlush(String sitNum, String status) {
        Map<String, String> map = null;
        if (StringUtils.isNotBlank(sitBetStatus)) {
            map = JSON.parseObject(sitBetStatus, Map.class);
        } else {
            map = new HashMap<>();
            map.put(sitNum, status);
        }
        map.put(sitNum, status);
        sitBetStatus = JSON.toJSONString(map);
    }

    public void checkDelay() {
        // 没人需要下注了
        if (allAllin() || onlyOneNotAllin() || allFoldOrAllin()) {
            if (StringUtils.isBlank(needDelay)) {
                if (GameStatus.TURN.name().equalsIgnoreCase(status)) {
                    setNeedDelay("1");
                } else if(GameStatus.FLOP.name().equalsIgnoreCase(status)) {
                    setNeedDelay("2");
                }
                else if(GameStatus.PRE_FLOP.name().equalsIgnoreCase(status)) {
                    setNeedDelay("3");
                }
            }
        }
    }

    public void setSitBetStatusIfExistsNotFlush(String sitNum, String status) {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            if (map.containsKey(sitNum)) {
                map.put(sitNum, status);
                sitBetStatus = JSON.toJSONString(map);
            }
        }
    }

    public boolean isPreFlop() {
       // 必须是pre_flop
       return GameStatus.PRE_FLOP.name().equalsIgnoreCase(status);
    }

    // 判断是否有玩家加过注
    public boolean hasUserRaise() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> entrySet = map.entrySet();
            if (entrySet == null || entrySet.size() == 0) {
                return false;
            }
            for (Map.Entry<String, String> entry : entrySet) {
                String userStatus = entry.getValue();
                if (GameEventType.RAISE.equalsIgnoreCase(userStatus)
                        || GameEventType.ALLIN.equalsIgnoreCase(userStatus)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断是否只有一个人没有弃牌
     * @return
     */
    public boolean onlyOneNotFold() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            if(valueCollection.size() == 1) {
                return true;
            }
            int index = 0;
            for (String status : valueCollection) {
                if (GameEventType.FOLD.equalsIgnoreCase(status)) {
                    index ++;
                }
            }
            if (valueCollection.size() > 1 && index == valueCollection.size() - 1) {
                return true;
            }
        }
        return false;
    }

    public boolean onlyOneNotAllin() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> entrySet = map.entrySet();
            int index = 0;
            int allinIndex = 0;
            String notAllinSit = "";
            int notFoldCount = 0; //筛选掉弃牌的人计数
            for (Map.Entry<String, String> entry : entrySet) {
                String userSitNum = entry.getKey();
                String userStatus = entry.getValue();
                if (GameEventType.FOLD.equalsIgnoreCase(userStatus)) {
                    continue;
                }
                notFoldCount++;
                if (GameEventType.RAISE.equalsIgnoreCase(userStatus) ||
                        GameEventType.CHECK.equalsIgnoreCase(userStatus) ||
                                     GameEventType.CALL.equalsIgnoreCase(userStatus)) {
                    index ++;
                    notAllinSit = userSitNum;
                }
                if (GameEventType.ALLIN.equalsIgnoreCase(userStatus)) {
                    allinIndex ++;
                }
            }
            if (index == 1 && allinIndex > 0 && allinIndex == notFoldCount - 1) {
                // 判断notAllinSit下注
                List<UserBetLog> allUserBetLogs = getUsersBetsLogList();
                UserBetLog notAllinBetLog =  getUserBetLog(notAllinSit);
                if (notAllinBetLog == null) {
                    return false;
                }
                for (UserBetLog userBetLog : allUserBetLogs) {
                     if (userBetLog.getRoundChips() + userBetLog.getTotalChips() >
                             notAllinBetLog.getRoundChips() + notAllinBetLog.getTotalChips()) {
                         return false;
                     }
                }
                return true;
            }
        }
        return false;
    }

    // 判断所有人是否已经allin
    public boolean allAllin() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                if (!GameEventType.ALLIN.equalsIgnoreCase(status)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    // 判断是否只剩最后一个人没有下注
    public boolean checkIsLastBetAndOnlyOneAllin(String nextBetSit) {
        if(onlyOneNotAllin2()) {
            if (StringUtils.isNotBlank(sitBetStatus)) {
                Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
                Set<Map.Entry<String, String>> entrySet = map.entrySet();
                String notAllinSit = "";
                for (Map.Entry<String, String> entry : entrySet) {
                    String userSitNum = entry.getKey();
                    String userStatus = entry.getValue();
                    if (GameEventType.FOLD.equalsIgnoreCase(userStatus)) {
                        continue;
                    }
                    if (GameEventType.RAISE.equalsIgnoreCase(userStatus) ||
                            GameEventType.CHECK.equalsIgnoreCase(userStatus) ||
                            GameEventType.CALL.equalsIgnoreCase(userStatus) ||
                            StringUtils.isBlank(userStatus)) { //小盲注位状态默认为空
                        notAllinSit = userSitNum;
                        break;
                    }
                }
                if (StringUtils.isNotBlank(notAllinSit) && notAllinSit.equalsIgnoreCase(nextBetSit)) {
                    // 判断这个人的筹码是否是最大的在onlyOneNotAllin中已经判断
                    String betStatus = getSitBetStatusBySitNum(nextBetSit);
                    if (StringUtils.isBlank(betStatus)) {
                        setSitBetStatusNotFlush(nextBetSit, GameEventType.CALL);
                    }
                    return true;
                }
            }
        }
        return false;
    }
    // 判断不足盲注后 allin
    public boolean onlyOneNotAllin2() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> entrySet = map.entrySet();
            int index = 0;
            int allinIndex = 0;
            String notAllinSit = "";
            int notFoldCount = 0; //筛选掉弃牌的人计数
            for (Map.Entry<String, String> entry : entrySet) {
                String userSitNum = entry.getKey();
                String userStatus = entry.getValue();
                if (GameEventType.FOLD.equalsIgnoreCase(userStatus)) {
                    continue;
                }
                notFoldCount++;
                if (GameEventType.RAISE.equalsIgnoreCase(userStatus) ||
                        GameEventType.CHECK.equalsIgnoreCase(userStatus) ||
                        GameEventType.CALL.equalsIgnoreCase(userStatus)
                        || StringUtils.isBlank(userStatus)) { // 小盲注位一开始没有下注状态
                    index ++;
                    notAllinSit = userSitNum;
                }
                if (GameEventType.ALLIN.equalsIgnoreCase(userStatus)) {
                    allinIndex ++;
                }
            }
            if (index == 1 && allinIndex == notFoldCount - 1) {
                // 判断notAllinSit下注
                List<UserBetLog> allUserBetLogs = getUsersBetsLogList();
                UserBetLog notAllinBetLog =  getUserBetLog(notAllinSit);
                if (notAllinBetLog == null) {
                    return false;
                }
                for (UserBetLog userBetLog : allUserBetLogs) {
                    if (userBetLog.getRoundChips() + userBetLog.getTotalChips() >
                            notAllinBetLog.getRoundChips() + notAllinBetLog.getTotalChips()) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
    public int notFoldCount() {
        int count = 0;
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                if (!GameEventType.FOLD.equalsIgnoreCase(status)) {
                    count++;
                }
            }
        }
        return count;
    }

    public boolean allFoldOrAllin() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                if (!GameEventType.ALLIN.equalsIgnoreCase(status)
                        && !GameEventType.FOLD.equalsIgnoreCase(status)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    //没有fold的全部allin
    public boolean notFoldAllAllin() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                // 玩家没有fold 并且没有allin
                if(!GameEventType.FOLD.equalsIgnoreCase(status) &&
                                !GameEventType.ALLIN.equalsIgnoreCase(status)) {
                        return false;
                }
            }
        }
        return true;
    }

    public boolean allFold() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                if (!GameEventType.FOLD.equalsIgnoreCase(status)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public List<String> allFoldSitNumList() {
        List<String> foldSitList = new ArrayList<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
            for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                 String val = mapEntry.getValue();
                 if (GameEventType.FOLD.equalsIgnoreCase(val)) {
                     foldSitList.add(mapEntry.getKey());
                 }
            }
        }
        return foldSitList;
    }
    // 没有弃牌的玩家userId
    public Set<String> notFoldUserIds() {
        Set<String> set = new HashSet<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
            Map<String, String> sitAndUserId = getSitBetUserIdMap();
            for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                String val = mapEntry.getValue();
                String sit = mapEntry.getKey();
                if (!GameEventType.FOLD.equalsIgnoreCase(val)) {
                    String userId = sitAndUserId.get(sit);
                    if (StringUtils.isNotBlank(sit)) {
                        set.add(userId);
                    }
                }
            }
        }
        return set;
    }

    // 获取所有未弃牌的座位和底牌信息
    public Map<String, String> allNotFoldSitNumsAndCards() {
        Map<String, String> resultMap = new HashMap<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> userCardsMap =  JSON.parseObject(userCards, Map.class);
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
            for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                 String val = mapEntry.getValue();
                 if (!GameEventType.FOLD.equalsIgnoreCase(val)) {
                     String cards = userCardsMap.get(mapEntry.getKey());
                     if (cards != null) {
                         resultMap.put(mapEntry.getKey(), cards);
                     }
                 }
            }
        }
        return resultMap;
    }

    // 获取所有的座位和底牌信息
    public Map<String, String> allSitNumsAndCards() {
        Map<String, String> resultMap = new HashMap<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> userCardsMap =  JSON.parseObject(userCards, Map.class);
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
            for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                String cards = userCardsMap.get(mapEntry.getKey());
                if (cards != null) {
                    resultMap.put(mapEntry.getKey(), cards);
                }
            }
        }
        return resultMap;
    }

    // 玩家下注
    public void userBet(String sitNum, long addChips) {
        if (addChips <= 0) {
            return;
        }
        if (StringUtils.isBlank(usersBetsLog)) {
            return;
        }
        try {
            List<UserBetLog> userBetLogs = getUsersBetsLogList();
            // 判断是否是第一个人在没回合下注
            boolean isFirstBet = checkIsFirstBet(userBetLogs);
            if(isFirstBet) {
               firstBetChips = Math.max(Long.parseLong(bigBlindMoney) , addChips) + "";
            }
            boolean isMaxBets = true;
            UserBetLog userBetLog = null;
            for (UserBetLog $userBetLog : userBetLogs) {
                if ($userBetLog.getSitNum().equalsIgnoreCase(sitNum)) {
                    userBetLog = $userBetLog;
                    break;
                }
            }
            if (userBetLog == null) {
                return;
            }
            long roundChips = userBetLog.getRoundChips();
            userBetLog.setRoundChips(Math.abs(addChips) + roundChips);

            //判断是否是最大下注
            for (UserBetLog $userBetLog : userBetLogs) {
                if ($userBetLog.getRoundChips() >= userBetLog.getRoundChips() &&
                        !$userBetLog.getSitNum().equalsIgnoreCase(userBetLog.getSitNum())) {
                    isMaxBets = false;
                }
            }
            if (isMaxBets) {
                setMaxBetSitNum(sitNum);
                setMaxBetSitNumChips(userBetLog.getRoundChips() + "");
                if (isFirstBet) {
                    setMaxBetSitNumAddChips(maxBetSitNumChips);
                } else {
                    long raise = Long.parseLong(maxBetSitNumChips) - Long.parseLong(firstBetChips);
                    setMaxBetSitNumAddChips(Math.max(Long.parseLong(bigBlindMoney), raise) + "");
                }
            }
            usersBetsLog = JSON.toJSONString(userBetLogs);
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addUserAllinSeq(String userId) {
        List<String> list = null;
        if (StringUtils.isBlank(userAllinSeq)) {
            list = new ArrayList<>();
        } else {
            list = JSON.parseArray(userAllinSeq, String.class);
        }
        list.add(userId);
        userAllinSeq = JSON.toJSONString(list);
    }

    public List<String> userAllinSeq() {
        if (StringUtils.isBlank(userAllinSeq)) {
            return new ArrayList<>();
        }
        return JSON.parseArray(userAllinSeq, String.class);
    }

    public void addUserStartMoney(String userId, long totalLeftChips) {
        Map<String, String> userStartMoney;
        if(StringUtils.isBlank(userStartLeftMoney)) {
           userStartMoney = new HashMap<>();
        } else {
            userStartMoney = JSON.parseObject(userStartLeftMoney, Map.class);
        }
        userStartMoney.put(userId, totalLeftChips + "");
        userStartLeftMoney = JSON.toJSONString(userStartMoney);
    }

    public Map<String, Long> userStartMoney() {
        Map<String, String> userStartMoney;
        if(StringUtils.isBlank(userStartLeftMoney)) {
            userStartMoney = new HashMap<>();
        } else {
            userStartMoney = JSON.parseObject(userStartLeftMoney, Map.class);
        }
        Map<String, Long> result = new HashMap<>();
        for (Map.Entry<String, String> entry : userStartMoney.entrySet()) {
            result.put(entry.getKey(), Long.parseLong(entry.getValue()));
        }
        return result;
    }

    public Long startUserTotalMoney() {
       Collection<Long> collection = userStartMoney().values();
       long allChips = 0;
       for (Long chips : collection) {
            allChips += chips;
       }
        return allChips;
    }

    public void updateUserBetRaiseLog(String sitNum, long addChips) {
        // 计算底池
        long total = getTotalPoolMoney();
        Map<String, String> userBetLogMap = null;
        if (userBetRaiseLog != null) {
            userBetLogMap = (Map<String, String>) JSON.parse(userBetRaiseLog);
        }
        if (userBetLogMap == null) {
            userBetLogMap = new HashMap<>();
        }
        if (total > 0 && userBetLogMap != null && StringUtils.isNotBlank(sitNum)) {
            String raiseStr = userBetLogMap.get(sitNum);
            java.text.DecimalFormat df = new java.text.DecimalFormat("#.##");
            String result = df.format((double) addChips / (double) total);
            if (StringUtils.isBlank(raiseStr)) {
                raiseStr = result;
            } else {
                raiseStr += ("," + result);
            }
            userBetLogMap.put(sitNum, raiseStr);
            userBetRaiseLog = JSON.toJSONString(userBetLogMap);
        }
    }

    public Map<String, String> userBetRaiseLogMap() {
        Map<String, String> userBetLogMap = null;
        if (userBetRaiseLog != null) {
            userBetLogMap = (Map<String, String>) JSON.parse(userBetRaiseLog);
        } else {
            userBetLogMap = new HashMap<>();
        }
        return userBetLogMap;
    }

    private boolean checkIsFirstBet(List<UserBetLog> userBetLogs) {
        for (UserBetLog $userBetLog : userBetLogs) {
             if ($userBetLog.getRoundChips() > 0)  {
                 return false;
             }
        }
        return true;
    }

    public void roundBetEnd() {
        //置空 玩家操作验证
        pwd = "";
        if (StringUtils.isNotBlank(usersBetsLog)) {
            try {

                List<UserBetLog> userBetLogs = getUsersBetsLogList();
                checkPool(userBetLogs);
                for (UserBetLog $userBetLog : userBetLogs) {
                    if ($userBetLog.getRoundChips() > 0) {
                        long total = $userBetLog.getTotalChips();
                        $userBetLog.setTotalChips(total + $userBetLog.getRoundChips());
                        $userBetLog.setRoundChips(0);
                    }
                }
                usersBetsLog = JSON.toJSONString(userBetLogs);
                firstBetChips = "0"; // 重置每次回合的第一个人下注
                maxBetSitNumAddChips = "0";
                // 重置没有 fold allin 玩家的状态
                if (StringUtils.isNotBlank(sitBetStatus)) {
                    // 记录这次的下注状态
                    lastSitBetStatus = sitBetStatus;
                    // 只有一个人没有弃牌,则不用重置状态
                    if (onlyOneNotAllin()) {
                        return;
                    }
                    Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
                    Set<Map.Entry<String, String>> entrySet = map.entrySet();
                    if (entrySet != null) {
                        // 判断是否只有一个人没有allin

                        for(Map.Entry<String, String> entry : entrySet) {
                            String value = entry.getValue();
                            if (!value.equalsIgnoreCase(GameEventType.FOLD)
                                    && !value.equalsIgnoreCase(GameEventType.ALLIN)) {
                                entry.setValue("");
                            }
                        }
                    }
                    sitBetStatus = JSON.toJSONString(map);
                }
            }  catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    // 获取最大的前一个加注额度
    public long getMaxPreRaise() {
      if (StringUtils.isBlank(maxBetSitNumAddChips)) {
          return 0;
      }
      return Long.parseLong(maxBetSitNumAddChips);
    }

    public boolean needShowAllCards() {
        // 大于1个以上的人全部allin，或者 只有一个人没allin其他的都allin(大于1)或弃牌
        if (notFoldAllAllin() || onlyOneNotAllin()) {
            // 判断没有弃牌的人数大于1
            if (notFoldCount() > 1) {
                return true;
            }
        }
        return false;
    }

    public List<Map<String, Object>> userNotFoldCards() {
         Map<String, String> allUserCards = getUsersCards();
         List<Map<String, Object>> resultList = new ArrayList<>();
         if (StringUtils.isNotBlank(sitBetStatus)) {
             Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
             Set<Map.Entry<String, String>> entrySet = map.entrySet();
             for (Map.Entry<String, String> entry : entrySet) {
                  String sitNum = entry.getKey();
                  String status = entry.getValue();
                  if (!GameEventType.FOLD.equalsIgnoreCase(status)) {
                      String cards = allUserCards.get(sitNum);
                      Map<String, Object> oneCards = new HashMap<>();
                      oneCards.put("sn", Integer.parseInt(sitNum));
                      oneCards.put("hand_cards", cards);
                      resultList.add(oneCards);
                  }
             }
         }
        return resultList;
    }
    // 分池
    @Deprecated
    public void splitPool(List<UserBetLog> userBetLogs) {
        // 先记录当前的分池信息
        lastBetPool = betPool;
        // 只剩一个人没弃牌
        if(onlyOneNotFold()) {
            addToLastPool(userBetLogs);
            return;
        }
        // 需要分池有人allin
        if (checkSplitPool(userBetLogs)) {
            TreeMap<String, Long> allBetsMap = getUserRoundChips(userBetLogs);
            if(allBetsMap.size() == 0) {
               return;
            }
            List<Map.Entry<String, Long>> entryList = getAllBetsEntryList(allBetsMap);
            while (entryList.size() > 0) {
               Map.Entry<String, Long> betEntry = entryList.remove(0);
               Long userChips = betEntry.getValue();
               if (userChips == null || userChips.longValue() <= 0) {
                   continue;
               }
            }
        }


    }
    // 把玩家下注合并到最后一个池子里面
    private void addToLastPool(List<UserBetLog> userBetLogs) {
        List<Map<String, String>> mapList = getBetPoolMapList();
        if (mapList == null) {
            mapList = new ArrayList<>();
        }
        Map<String, String> nearMap = null;
        boolean needAddToList = false;
        if (mapList.size() > 0) {
            nearMap = mapList.get(mapList.size() - 1);
        }
        else {
            nearMap = new HashMap<>();
            needAddToList = true;
        }
        // 玩家下注日志
        for (UserBetLog ubl : userBetLogs) {
            if (ubl.getRoundChips() <= 0) {
                continue;
            }
            String sitNum = ubl.getSitNum();
            String userBet = nearMap.get(sitNum);
            if (StringUtils.isNotBlank(userBet)) {
                userBet = (Long.parseLong(userBet) + ubl.getRoundChips()) + "";
            }
            else {
                userBet = ubl.getRoundChips() + "";
            }
            nearMap.put(sitNum, userBet);
        }
        if (needAddToList) {
            mapList.add(nearMap);
        }
        betPool = JSON.toJSONString(mapList);
    }

    // 判断是否需要分池
    private boolean checkSplitPool(List<UserBetLog> userBetLogs) {
        Map<String, String> betStatusMap = getSitBetStatusMap();
        for (UserBetLog $userBetLog : userBetLogs) {
            String sitNum = $userBetLog.getSitNum();
            if (StringUtils.isBlank(sitNum)) {
                continue;
            }
            String status = betStatusMap.get(sitNum);
            if(GameEventType.ALLIN.equalsIgnoreCase(status)) {
               return true;
            }
        }
        return false;
    }

    private TreeMap<String, Long> getUserRoundChips(List<UserBetLog> userBetLogs) {
        TreeMap<String, Long> allBetsMap = new TreeMap<>();
        for (UserBetLog $userBetLog : userBetLogs) {
            long roundChips = $userBetLog.getRoundChips();
            if (roundChips > 0) {
                String sitNum = $userBetLog.getSitNum();
                if (StringUtils.isBlank(sitNum)) {
                    continue;
                }
                allBetsMap.put(sitNum, roundChips);
            }
        }
        return allBetsMap;
    }

    private List<Map.Entry<String, Long>> getAllBetsEntryList(TreeMap<String, Long> allBetsMap) {
        List<Map.Entry<String, Long>> allBetsList = new ArrayList(allBetsMap.entrySet());
        // 根据value从小到大排序
        Collections.sort(allBetsList, new Comparator<Map.Entry<String, Long>>(){
            public int compare(Map.Entry<String, Long> o1, Map.Entry<String, Long> o2) {
                long result = o1.getValue() - o2.getValue();
                if(result > 0) {
                    return 1;
                } else if (result < 0) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        return allBetsList;
    }

    //TODO 大盲注和小盲注问题，分池时候allin 小于一个小忙注问题
    public void checkPool(List<UserBetLog> userBetLogs) {
        // 先记录当前的分池信息
        lastBetPool = betPool;
        String tmpLastBetPool = lastBetPool;
        // 只剩一个人没弃牌
        if(onlyOneNotFold()) {
            oneNotFoldCheckPool(userBetLogs);
            return;
        }

        if (StringUtils.isBlank(sitBetStatus)) {
            return;
        }
        Map<String, String> betsStatusMap = JSON.parseObject(sitBetStatus, Map.class);
        boolean hasAllin = false;
        Map<String, String> allBetsMap = new TreeMap<>();
        for (UserBetLog $userBetLog : userBetLogs) {
            long roundChips = $userBetLog.getRoundChips();
            if (roundChips > 0) {
                String sitNum = $userBetLog.getSitNum();
                String status = betsStatusMap.get(sitNum);
                 // 1, 5 弃牌  2,40   3,30 allin 4 40 根
                if(GameEventType.ALLIN.equalsIgnoreCase(status)) {
                    hasAllin = true;
                }
                allBetsMap.put(sitNum, roundChips + "");
            }
        }
        if(allBetsMap.size() == 0) {
           return;
        }
        // 分池
//        if(hasAllin) {
            if (allBetsMap.size() > 0) {
                List<Map.Entry<String, String>> allBetsList = new ArrayList(allBetsMap.entrySet());
                // 根据value从小到大排序
                Collections.sort(allBetsList, new Comparator<Map.Entry<String, String>>(){
                    public int compare(Map.Entry<String, String> o1, Map.Entry<String, String> o2) {
                         long result = Long.parseLong(o1.getValue()) -  Long.parseLong(o2.getValue());
                         if(result > 0) {
                             return 1;
                         } else if (result < 0) {
                             return -1;
                         } else {
                             return 0;
                         }
                    }
                });
                List<Map<String, String>> poolList = null;
                if (StringUtils.isBlank(betPool)) {
                    poolList = new ArrayList<>();
                } else {
                    poolList = JSON.parseObject(betPool, List.class);
                }

                // 每次下注回合结束后第一次分池是否需要合并
                boolean needMerger = false;
                Map<String, String> tmpPoolMap = new HashMap<>();
                while (allBetsList.size() > 0) {
                    // 最新的一个池子
                    Map<String, String> lastPoolMap = null;
                    if (poolList.size() > 0) {
                        lastPoolMap = poolList.get(poolList.size() - 1);
                    }
                    // 判断本次分池是否需要和上一个池子合并
                    needMerger = checkNewPoolMerger(lastPoolMap, allBetsList);

                    int betListSize = allBetsList.size();
                    Map.Entry<String, String> betEntry = allBetsList.remove(0);
                    if (betListSize > 0) {
                        long beginPool = Long.parseLong(betEntry.getValue());
                        if (beginPool <= 0) {
                            continue;
                        }
                        Map<String, String> map = new HashMap<>();
                        //先放入自己
                        map.put(betEntry.getKey(), betEntry.getValue());
                        for (Map.Entry<String, String> userBetEntry : allBetsList) {
                             long userBet = Long.parseLong(userBetEntry.getValue());
                             map.put(userBetEntry.getKey(), beginPool + "");
                             userBetEntry.setValue((userBet - beginPool) + "");
                        }

                        // 把当前分出来的筹码累加入到临时的池子
                        for(Map.Entry<String, String> entry : map.entrySet()) {
                            String sitNum = entry.getKey();
                            long bet =  Long.parseLong(entry.getValue());
                            if (bet > 0) {
                                String tmpBet = tmpPoolMap.get(sitNum);
                                String newBet = bet + "";
                                if (StringUtils.isNotBlank(tmpBet)) {
                                    newBet = (bet + Long.parseLong(tmpBet)) + "";
                                }
                                tmpPoolMap.put(sitNum, newBet);
                            }
                        }
                        if (!needMerger) { // 不需要把池子merger到上一个回合的池中
                            Map<String ,String> copySplitPoolMap = new HashMap<>();
                            copySplitPoolMap.putAll(tmpPoolMap);
                            poolList.add(copySplitPoolMap);
                        }
                        else {
                            // 把当前分池合并到上个的最后一个池子
                            for(Map.Entry<String, String> entry : tmpPoolMap.entrySet()) {
                                long mapVal = Long.parseLong(lastPoolMap.get(entry.getKey()));
                                if (mapVal > 0) {
                                    mapVal +=  Long.parseLong(entry.getValue());
                                }
                                lastPoolMap.put(entry.getKey(), mapVal + "");
                            }
                        }
                        //清除掉数据
                        tmpPoolMap.clear();
                    }
                }
                if (tmpPoolMap.size() > 0) {
                    poolList.add(tmpPoolMap);
                }
                betPool = JSON.toJSONString(poolList);
            }
    }

    // 确认底池合并
    private boolean checkPoolMerger(Map<String, String> lastBetPoolMap) {
        if (StringUtils.isBlank(lastSitBetStatus)) {
            return true;
        }
        // 上一次玩家下注
        Map<String, String> lastSitBetStatusMap = JSON.parseObject(lastSitBetStatus, Map.class);
        Set<String> sitNums = lastBetPoolMap.keySet();
        for (String sitNum : sitNums) {
             String status = lastSitBetStatusMap.get(sitNum);
             // 上次的池子里面有allin的本次分池不能合并到上次池子里
             if (GameEventType.ALLIN.equalsIgnoreCase(status)) {
                 return false;
             }
        }
        return true;
    }

    // 确认底池合并,上次池子里面下注的人数量和这次是否相同
    private boolean checkNewPoolMerger(Map<String, String> lastBetPoolMap,
                                       List<Map.Entry<String, String>> currentBetList) {

        if (lastBetPoolMap == null) {
            // 上次池子没有人下注，表示第一次，不需要有合并
            return false;
        }
        // 当前还有下注筹码的人数
        int betCnt = 0;
        int currentBetCnt = 0;
        for (Map.Entry<String, String> entry : currentBetList) {
            String sit = entry.getKey();
            String status = getSitBetStatusBySitNum(sit);
            // 排除弃牌的
            if (GameEventType.FOLD.equalsIgnoreCase(status)) { // 筛选掉弃牌
                continue;
            }
            currentBetCnt ++;
        }

        Set<Map.Entry<String, String>> entrySet = lastBetPoolMap.entrySet();
        for (Map.Entry<String, String> betEntry : entrySet) {
             String bet = betEntry.getValue();
             String sit = betEntry.getKey();
             String status = getSitBetStatusBySitNum(sit);

             // 排除弃牌的
             if (GameEventType.FOLD.equalsIgnoreCase(status)) {
                 continue;
             }
             if (!"0".equalsIgnoreCase(bet)) {
                 betCnt++;
             }
        }
        // 这次池子里面下注的人数和上次池子里面的人数相等则需要合并
        if (betCnt == currentBetCnt) {
            return true;
        }
        return false;
    }

    public void oneNotFoldCheckPool(List<UserBetLog> userBetLogs) {
        List<Map<String, String>> mapList = null;
        if(StringUtils.isNotBlank(betPool)) {
            mapList = JSON.parseObject(betPool, List.class);
        } else {
            mapList = new ArrayList<>();
        }
        Map<String, String> bottomBetMap = null;
        if (mapList.size() > 0) {
            int index = mapList.size() - 1;
            bottomBetMap = mapList.get(index);
            for (UserBetLog userBetLog : userBetLogs) {
                if (userBetLog.getRoundChips() > 0) {
                    Long userBet = Long.parseLong(bottomBetMap.get(userBetLog.getSitNum()));
                    if (userBet != null) {
                        userBet += userBetLog.getRoundChips();
                    }
                    bottomBetMap.put(userBetLog.getSitNum(), userBet + "");
                }
            }
        } else {
            bottomBetMap = new HashMap<>();
            for (UserBetLog userBetLog : userBetLogs) {
                if (userBetLog.getRoundChips() > 0) {
                    bottomBetMap.put(userBetLog.getSitNum(), userBetLog.getRoundChips() + "");
                }
            }
            mapList.add(bottomBetMap);
        }
        betPool = JSON.toJSONString(mapList);
    }

    public Map<String, String> getUsersCards() {
        Map<String, String> mapCards = new HashMap<>();
        if (StringUtils.isNotBlank(userCards)) {
            mapCards = JSON.parseObject(userCards, Map.class);
        }
        return mapCards;
    }

    public String getUserHandCards(String sitNum) {
        Map<String, String> usersCards = getUsersCards();
        if (usersCards != null) {
            return usersCards.get(sitNum);
        }
        return null;
    }

    public List<String> getDeskCardList() {
        if (StringUtils.isNotBlank(deskCards)) {
            List<String> list = JSON.parseArray(deskCards, String.class);
            return list;
        }
        return null;
    }

    public List<Long> betPoolList() {
        List<Long> list = new ArrayList<>();
        if (StringUtils.isNotBlank(betPool)) {
            List<Map<String, String>> mapList = JSON.parseObject(betPool, List.class);
            for (Map<String, String> map : mapList) {
                 Collection<String> values =  map.values();
                 long totalVal = 0;
                 for (String value : values) {
                      totalVal += Long.parseLong(value);
                 }
                 list.add(totalVal);
            }
        }
        return list;
    }

    public List<Map<String, String>> getBetPoolMapList() {
        if (StringUtils.isNotBlank(betPool)) {
            List<Map<String, String>> mapList = JSON.parseObject(betPool, List.class);
            return mapList;
        }
        return null;
    }

    public UserBetLog getUserBetLog(String sitNum) {
        if (StringUtils.isBlank(sitNum)) {
            return null;
        }
        List<UserBetLog> userBetLogs = getUsersBetsLogList();
        for (UserBetLog userBetLog : userBetLogs) {
             if (userBetLog.getSitNum().equalsIgnoreCase(sitNum)) {
                return userBetLog;
             }
        }
        return null;
    }

    public void delSelf() {
        RedisUtil.del(key());
    }

    public List<UserBetLog> getUsersBetsLogList() {
        if (StringUtils.isNotBlank(usersBetsLog)) {
            return JSON.parseArray(usersBetsLog, UserBetLog.class);
        }
        return new ArrayList<>();
    }

    private String getFastRaise(long userLeftChips, long needAdd) {
        // 没人加过注玩家可以， 三倍大盲, 五倍大盲
        if (isPreFlop() && !hasUserRaise()) {
            return "110000"; //最后一位表示是否加过注0 否 1是
        }
        long totalPoolMoney = getTotalPoolMoney();
        long halfPool = totalPoolMoney / 2;
        long mod1 = halfPool % Long.parseLong(bigBlindMoney);
        if(mod1 != 0) {
            halfPool = halfPool - mod1 + Long.parseLong(bigBlindMoney);
        }
        String fastRaise = "00";
        if (halfPool > userLeftChips || needAdd > halfPool){
            fastRaise += "0";
        }
        else {
            fastRaise += "1";
        }
        long twoThirdsPool = (totalPoolMoney * 2) / 3;
        long mod2 = twoThirdsPool % Long.parseLong(bigBlindMoney);
        if(mod2 != 0) {
           twoThirdsPool = twoThirdsPool - mod2 + Long.parseLong(bigBlindMoney);
        }
        if (twoThirdsPool > userLeftChips || needAdd > twoThirdsPool){
            fastRaise += "0";
        }
        else {
            fastRaise += "1";
        }
        long onePool = totalPoolMoney;
        if (onePool > userLeftChips || needAdd > onePool){
            fastRaise += "0";
        }
        else {
            fastRaise += "1";
        }
        fastRaise += "1";
        return fastRaise;
    }

    // 获取玩家能够就行的操作 四位操作符 1 表示可以 0 表示不可以  弃牌,看牌,跟注,加注,全押
    public String[] getUserCanOps(String userId, String currentBetSitNum) {
        UserChips userChips = UserChips.load(userId, roomId);
        if (userChips == null) {
            return new String[] { "00000", "0", "0", "0", "000000"};
        }
        long userLeftChips = userChips.getChips();
        // 玩家可以进行的操作
        long maxBetSitChips = Long.parseLong(maxBetSitNumChips);
        // 每局开始的时候maxBetSitChips都是为0，所以这里要做一下判断
        if (maxBetSitChips != 0) {
            maxBetSitChips = Math.max(Long.parseLong(bigBlindMoney), maxBetSitChips);
        }
        // 计算已经下注额
        long userAlreadyBet = 0;
        // 设置call的值
        UserBetLog userBetLog = getUserBetLog(currentBetSitNum);
        if (userBetLog != null) {
            userAlreadyBet = userBetLog.getRoundChips();
        }
        long callChips = maxBetSitChips - userAlreadyBet;
        long bigBlind = Long.parseLong(bigBlindMoney);

        //最大加注额等于一个大盲注，则下一个加注人的最少加注为2个大盲注
        long minRaise = 0;
        if (maxBetSitChips == bigBlind) {
            minRaise = maxBetSitChips * 2;
        }
        // 玩家已经下的筹码
        long userAddChips = maxBetSitChips - callChips;

        //大盲注在pre-flop下注
        if(GameStatus.PRE_FLOP.name().equalsIgnoreCase(status)) {
            // 当前下注的是
            if (currentBetSitNum.equalsIgnoreCase(maxBetSitNum)
                    && maxBetSitNum.equalsIgnoreCase(bigBlindNum)) {
                String fastRaise = getFastRaise(userLeftChips, Long.parseLong(bigBlindMoney));
                return new String[] {"11011",  bigBlindMoney, "0", userChips.getChips() + "", fastRaise};
            }
        }
        if (maxBetSitChips == 0 ) {// 没有任何人下注
            // 最小加注
            minRaise = bigBlind;
            long needAdd = minRaise - userAddChips;
            String fastRaise = getFastRaise(userLeftChips, needAdd);
            // TODO print日志
            if (needAdd % Long.parseLong(smallBlindMoney) != 0) {
                System.out.println("===errprint:==1==" + JSON.toJSONString(this));
            }
            return new String[] {"11011", Math.max(Long.parseLong(bigBlindMoney), needAdd)  + "", "0", userChips.getChips() + "", fastRaise};
        }
        if (maxBetSitChips >= userLeftChips + userAlreadyBet) {// 最大下注大于或等于用户剩下的筹码与以下筹码之和，玩家只能放弃和allin
            return new String[] { "10001", "0", "0", userChips.getChips() + "", "000001"};
        }
        if (minRaise == 0) {
           //  minRaise = maxBetSitChips * 2 - bigBlind;
             minRaise = maxBetSitChips + getMaxPreRaise();
        }

        // 判断当前玩家是否是最后一个没有allin的 玩家只需要跟注
        if (checkOnlyCurrentNotAllin(currentBetSitNum)) {
            return new String[]{"10100", "0", callChips + "", userChips.getChips() + "", "000001"};
        }

        if (minRaise > userLeftChips) { // 玩家剩余筹码小于最小加注，玩家只能是跟注或者allin
            return new String[] { "10101", "0", callChips + "", userChips.getChips() + "", "000001"};
        }


        long needAdd = minRaise - userAddChips;
        String fastRaise = getFastRaise(userLeftChips, needAdd);
        // TODO print日志
        if (needAdd % Long.parseLong(smallBlindMoney) != 0) {
            System.out.println("===errprint:===2=" + JSON.toJSONString(this));
        }
        return new String[]{"10111", Math.max(Long.parseLong(bigBlindMoney), needAdd)  + "", callChips + "", userChips.getChips() + "", fastRaise};
    }

    // 判断是否只是该玩家没有allin
    private boolean checkOnlyCurrentNotAllin(String sitNum) {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            String userStatus = map.get(sitNum);
            if (!GameEventType.FOLD.equalsIgnoreCase(userStatus)) {
                Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
                for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                     String userSitNum = mapEntry.getKey();
                     String val = mapEntry.getValue();
                     // 发现除了当前玩家以外的人没有allin
                     if (!userSitNum.equalsIgnoreCase(sitNum)
                             && !GameEventType.ALLIN.equalsIgnoreCase(val)) {
                         return false;
                     }
                }
                return true;
            }
        }
        return false;
    }

    public long getTotalPoolMoney() {
        List<UserBetLog> userBetLogList = getUsersBetsLogList();
        if (userBetLogList != null && userBetLogList.size() > 0) {
            long totalPool = 0;
            for (UserBetLog userBetLog : userBetLogList) {
                 if (userBetLog != null) {
                     long total = userBetLog.getTotalChips();
                     long round = userBetLog.getRoundChips();
                     totalPool += (total + round);
                 }
            }
            return totalPool;
        }
        return 0;
    }

    public List<Map<String, Object>> allUserBetStatus() {
        List<Map<String, Object >> resultList = new ArrayList<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> entrySet = map.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                 String sitNum = entry.getKey();
                 String status = entry.getValue();
                 Map<String, Object> $map = new HashMap<>();
                 $map.put("sn", Integer.valueOf(sitNum));
                 $map.put("status", status);
                 resultList.add($map);
            }
        }
        return resultList;
    }

    public Map<String, String> getSitBetStatusMap() {
        if (StringUtils.isBlank(sitBetStatus)) {
            return null;
        }
        return JSON.parseObject(sitBetStatus, Map.class);
    }

    public Map<String, String> getSitBetUserIdMap() {
        if (StringUtils.isBlank(sitUserIds)) {
            return null;
        }
        return JSON.parseObject(sitUserIds, Map.class);
    }

    public String getSitBetStatusBySitNum(String sitNum) {
        Map<String, String> sitBetMap = getSitBetStatusMap();
        if (sitBetMap != null) {
            return sitBetMap.get(sitNum);
        }
        return null;
    }

    public String randomPwd() {
        String id = UUID.randomUUID().toString().replace("-", "");
        pwd = id;
        return id;
    }

    public boolean checkPwd(String userPwd) {
        if (StringUtils.isNotBlank(pwd) && StringUtils.isNotBlank(userPwd)) {
            return pwd.equalsIgnoreCase(userPwd);
        }
        return false;
    }

    private boolean checkSit(String userSit) {
        if (StringUtils.isNotBlank(currentBet) && StringUtils.isNotBlank(userSit)) {
            return currentBet.equalsIgnoreCase(userSit);
        }
        return false;
    }

    public boolean checkPwdAndSit(String userPwd, String userSit) {
        return checkPwd(userPwd) && checkSit(userSit);
    }

    public String getInningId() {
        if (inningId == null) {
            return "";
        }
        return inningId;
    }

    public Map<String, String> allFoldSitAndUserIds() {
        Map<String, String> retMap = new HashMap<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
            Map<String, String> sitAndUserId = getSitBetUserIdMap();
            for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                String val = mapEntry.getValue();
                String sit = mapEntry.getKey();
                if (GameEventType.FOLD.equalsIgnoreCase(val)) {
                    String userId = sitAndUserId.get(sit);
                    if (StringUtils.isNotBlank(sit)) {
                        retMap.put(sit, userId);
                    }
                }
            }
        }
        return retMap;
    }

    public List<AllinInf> userAllInfos() {
        if (StringUtils.isBlank(userAllinSeq)) {
            return Lists.newArrayList();
        } else {
            return JSON.parseArray(userAllinSeq, AllinInf.class);
        }
    }

    public GameStartEvent.Game game() {
        if (StringUtils.isNotBlank(gameInfo)) {
            return JSON.parseObject(gameInfo, GameStartEvent.Game.class);
        }
        return null;
    }
    // 是否在倒计时
    public boolean isCountDown() {
        return StringUtils.isNotBlank(startMills) && System.currentTimeMillis() < Long.parseLong(startMills);
    }

    public void addRebuyUserIds(String userId) {
        if (StringUtils.isNotBlank(rebuyUserIds)) {
            List<String> userIds = JSON.parseArray(rebuyUserIds, String.class);
            if (userIds.contains(userId)) {
                return;
            }
            userIds.add(userId);
            rebuyUserIds = JSON.toJSONString(userIds);
        } else {
            List<String> userIds = new ArrayList<>();
            userIds.add(userId);
            rebuyUserIds = JSON.toJSONString(userIds);
        }
    }

    public int reduceRebuyUserId(String userId) {
        if (StringUtils.isBlank(rebuyUserIds)) {
            return 0;
        }
        List<String> userIds = JSON.parseArray(rebuyUserIds, String.class);
        userIds.remove(userId);
        rebuyUserIds = JSON.toJSONString(userIds);
        return userIds.size();
    }

    public boolean hasUserRebuy(String userId) {
        if (StringUtils.isBlank(rebuyUserIds)) {
            return false;
        }
        List<String> userIds = JSON.parseArray(rebuyUserIds, String.class);
        return userIds.contains(userId);
    }

    public boolean hasOtherUserRebuy(String selfUserId) {
        if (StringUtils.isBlank(rebuyUserIds)) {
            return false;
        }
        List<String> userIds = JSON.parseArray(rebuyUserIds, String.class);
        userIds.remove(selfUserId);
        return userIds.size() > 0;
    }

    public void addGameOverUserId(String userId) {
        List<String> overUserIds;
        if (StringUtils.isBlank(overList)) {
            overUserIds = new ArrayList<>();
        } else {
            overUserIds = JSON.parseArray(overList, String.class);
        }
        if (overUserIds.contains(userId)) {
            return;
        }
        overUserIds.add(userId);
        overList = JSON.toJSONString(overUserIds);
    }

    public List<String> overUserList() {
        List<String> overUserIds;
        if (StringUtils.isBlank(overList)) {
            overUserIds = new ArrayList<>();
        } else {
            overUserIds = JSON.parseArray(overList, String.class);
        }
        return overUserIds;
    }

    public void removeGameOverUserId(String userId) {
        if (StringUtils.isBlank(overList)) {
            return;
        }
        List<String> overUserIds = JSON.parseArray(overList, String.class);
        overUserIds.remove(userId);
        overList = JSON.toJSONString(overUserIds);
    }

    public List<String> roundGameOverUserId() {
        if (StringUtils.isBlank(overList)) {
            return new ArrayList<>();
        }
        return JSON.parseArray(overList, String.class);
    }

    public void addLeftUserChips(String userId, Long leftChips) {
         Map<String, String> chipsMap;
         if (StringUtils.isBlank(userLeftChips)) {
             chipsMap = new HashMap<>();
         } else {
             chipsMap = JSON.parseObject(userLeftChips, Map.class);
         }
         chipsMap.put(userId, leftChips + "");
         userLeftChips = JSON.toJSONString(chipsMap);
    }

    public Map<String, Long> userLeftChipsMap() {
        Map<String, String> chipsMap;
        if (StringUtils.isBlank(userLeftChips)) {
            chipsMap = new HashMap<>();
        } else {
            chipsMap = JSON.parseObject(userLeftChips, Map.class);
        }
        Map<String, Long> longChips = new HashMap<>();
        for (Map.Entry<String, String> entry : chipsMap.entrySet()) {
             longChips.put(entry.getKey(), Long.parseLong(entry.getValue()));
        }
        return longChips;
    }

    public void addUserAnte(String sitNum, long ante) {
        Map<String, String> userAnteMap;
        if (StringUtils.isBlank(anteMap)) {
            userAnteMap = new HashMap<>();
        } else {
            userAnteMap = JSON.parseObject(anteMap, Map.class);
        }
        userAnteMap.put(sitNum, ante + "");
        anteMap = JSON.toJSONString(userAnteMap);
    }

    public long userAnte(String sitNum) {
        Map<String, String> userAnteMap;
        if (StringUtils.isBlank(anteMap)) {
            userAnteMap = new HashMap<>();
        } else {
            userAnteMap = JSON.parseObject(anteMap, Map.class);
        }
        String betAnte = userAnteMap.get(sitNum);
        if (betAnte == null) {
            return 0;
        }
        return Long.parseLong(betAnte) ;
    }

    public long totalAnte() {
        Map<String, String> userAnteMap;
        if (StringUtils.isBlank(anteMap)) {
            userAnteMap = new HashMap<>();
        } else {
            userAnteMap = JSON.parseObject(anteMap, Map.class);
        }
        if (userAnteMap.size() > 0) {
            Collection<String> vals = userAnteMap.values();
            long totalAnte = 0;
            for(String val : vals) {
                totalAnte += Long.parseLong(val);
            }
            return totalAnte;
        }
        return 0l;
    }
    public boolean isDelayingSign() {
        LOG.info("reBuyInCnt:" + reBuyInCnt +",reBuyInCnt:" + reBuyInCnt
                + ",currentBlindLevel:" + currentBlindLevel + ",maxCanRebuyBlindLvl:" + maxCanRebuyBlindLvl);
        if (StringUtils.isBlank(reBuyInCnt) || Integer.parseInt(reBuyInCnt) <= 0) {
            return false;
        }
        if (StringUtils.isBlank(currentBlindLevel) || StringUtils.isBlank(maxCanRebuyBlindLvl))  {
            return false;
        }
        return Long.parseLong(currentBlindLevel) <= Long.parseLong(maxCanRebuyBlindLvl);
    }

    public boolean canRebuy() {
         return isDelayingSign();
    }

    public String getOverList() {
        return overList;
    }

    public void setOverList(String overList) {
        this.overList = overList;
    }

    public boolean isStarted() {
        return "1".equals(started);
    }

    public void gameStop() {
        started = "0";
    }

    public void gameStart() {
        started = "1";
    }

    public String getTableId() {
        return tableId;
    }

    public void setInningId(String inningId) {
        this.inningId = inningId;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    public String getSmallBlindNum() {
        return smallBlindNum;
    }

    public void setSmallBlindNum(String smallBlindNum) {
        this.smallBlindNum = smallBlindNum;
    }

    public String getBigBlindNum() {
        return bigBlindNum;
    }

    public void setBigBlindNum(String bigBlindNum) {
        this.bigBlindNum = bigBlindNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        System.out.println("setStatus========" + status + ",===tableId:" + tableId);
        this.status = status;
    }

    public String getCurrentBet() {
        return currentBet;
    }

    public void setCurrentBet(String currentBet) {
        this.currentBet = currentBet;
    }

    public String getMaxBetSitNum() {
        return maxBetSitNum;
    }

    public void setMaxBetSitNum(String maxBetSitNum) {
        this.maxBetSitNum = maxBetSitNum;
    }

    public String getCards() {
        return cards;
    }

    public void setCards(String cards) {
        this.cards = cards;
    }

    public String getUserCards() {
        return userCards;
    }

    public void setUserCards(String userCards) {
        this.userCards = userCards;
    }

    public String getDeskCards() {
        return deskCards;
    }

    public void setDeskCards(String deskCards) {
        this.deskCards = deskCards;
    }

    public String getLastUpdateTimes() {
        return lastUpdateTimes;
    }

    public void setLastUpdateTimes(String lastUpdateTimes) {
        this.lastUpdateTimes = lastUpdateTimes;
    }

    public String getMaxBetSitNumChips() {
        return maxBetSitNumChips;
    }

    public void setMaxBetSitNumChips(String maxBetSitNumChips) {
        this.maxBetSitNumChips = maxBetSitNumChips;
    }

    public String getCurrentBetTimes() {
        return currentBetTimes;
    }

    public void setCurrentBetTimes(String currentBetTimes) {
        this.currentBetTimes = currentBetTimes;
    }

    public String getSitBetStatus() {
        return sitBetStatus;
    }

    public void setSitBetStatus(String sitBetStatus) {
        this.sitBetStatus = sitBetStatus;
    }

    public String getBigBlindMoney() {
        return bigBlindMoney;
    }

    public void setBigBlindMoney(String bigBlindMoney) {
        this.bigBlindMoney = bigBlindMoney;
    }

    public String getSmallBlindMoney() {
        return smallBlindMoney;
    }

    public void setSmallBlindMoney(String smallBlindMoney) {
        this.smallBlindMoney = smallBlindMoney;
    }

    public String getUsersBetsLog() {
        return usersBetsLog;
    }

    public void setUsersBetsLog(String usersBetsLog) {
        this.usersBetsLog = usersBetsLog;
    }

    public String getBetPool() {
        return betPool;
    }

    public void setBetPool(String betPool) {
        this.betPool = betPool;
    }

    public String getIndexCount() {
        if (StringUtils.isBlank(indexCount)) {
            indexCount = "1";
        }
        return indexCount;
    }

    public void setIndexCount(String indexCount) {
        this.indexCount = indexCount;
    }

    public String getLastBetPool() {
        return lastBetPool;
    }

    public void setLastBetPool(String lastBetPool) {
        this.lastBetPool = lastBetPool;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLastSitBetStatus() {
        return lastSitBetStatus;
    }

    public void setLastSitBetStatus(String lastSitBetStatus) {
        this.lastSitBetStatus = lastSitBetStatus;
    }

    public String getUserOnlineTaskId() {
        return userOnlineTaskId;
    }

    public void setUserOnlineTaskId(String userOnlineTaskId) {
        this.userOnlineTaskId = userOnlineTaskId;
    }

    public String getNeedDelay() {
        return needDelay;
    }

    public void setNeedDelay(String needDelay) {
        this.needDelay = needDelay;
    }

    public String getBuyIn() {
        return buyIn;
    }

    public void setBuyIn(String buyIn) {
        this.buyIn = buyIn;
    }

    public String getSitUserIds() {
        return sitUserIds;
    }

    public void setSitUserIds(String sitUserIds) {
        this.sitUserIds = sitUserIds;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getFirstBetChips() {
        return firstBetChips;
    }

    public void setFirstBetChips(String firstBetChips) {
        this.firstBetChips = firstBetChips;
    }

    public String getMaxBetSitNumAddChips() {
        return maxBetSitNumAddChips;
    }

    public void setMaxBetSitNumAddChips(String maxBetSitNumAddChips) {
        this.maxBetSitNumAddChips = maxBetSitNumAddChips;
    }

    public String getUserBetRaiseLog() {
        return userBetRaiseLog;
    }

    public void setUserBetRaiseLog(String userBetRaiseLog) {
        this.userBetRaiseLog = userBetRaiseLog;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getNextUpBlindTimes() {
        return nextUpBlindTimes;
    }

    public void setNextUpBlindTimes(String nextUpBlindTimes) {
        this.nextUpBlindTimes = nextUpBlindTimes;
    }

    public String getCurrentBlindLevel() {
        return currentBlindLevel;
    }

    public void setCurrentBlindLevel(String currentBlindLevel) {
        this.currentBlindLevel = currentBlindLevel;
    }

    public String getStarted() {
        return started;
    }

    public void setStarted(String started) {
        this.started = started;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getStartMills() {
        return startMills;
    }

    public void setStartMills(String startMills) {
        this.startMills = startMills;
    }

    public String getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(String gameInfo) {
        this.gameInfo = gameInfo;
    }

    public String getAnte() {
        return ante;
    }

    public void setAnte(String ante) {
        this.ante = ante;
    }

    public String getMinUsers() {
        return minUsers;
    }

    public void setMinUsers(String minUsers) {
        this.minUsers = minUsers;
    }

    public String getMaxUsers() {
        return maxUsers;
    }

    public void setMaxUsers(String maxUsers) {
        this.maxUsers = maxUsers;
    }

    public String getLeftUsers() {
        return leftUsers;
    }

    public void setLeftUsers(String leftUsers) {
        this.leftUsers = leftUsers;
    }

    public String getAverageChips() {
        return averageChips;
    }

    public void setAverageChips(String averageChips) {
        this.averageChips = averageChips;
    }

    public String getMaxCanRebuyBlindLvl() {
        return maxCanRebuyBlindLvl;
    }

    public void setMaxCanRebuyBlindLvl(String maxCanRebuyBlindLvl) {
        this.maxCanRebuyBlindLvl = maxCanRebuyBlindLvl;
    }

    public String getReBuyInCnt() {
        return reBuyInCnt;
    }

    public void setReBuyInCnt(String reBuyInCnt) {
        this.reBuyInCnt = reBuyInCnt;
    }

    public String getRebuyUserIds() {
        return rebuyUserIds;
    }

    public void setRebuyUserIds(String rebuyUserIds) {
        this.rebuyUserIds = rebuyUserIds;
    }

    public String getUserLeftChips() {
        return userLeftChips;
    }

    public void setUserLeftChips(String userLeftChips) {
        this.userLeftChips = userLeftChips;
    }

    public String getAnteMap() {
        return anteMap;
    }

    public void setAnteMap(String anteMap) {
        this.anteMap = anteMap;
    }

    public String getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(String totalUsers) {
        this.totalUsers = totalUsers;
    }

    public String getRebuyStartMills() {
        return rebuyStartMills;
    }

    public void setRebuyStartMills(String rebuyStartMills) {
        this.rebuyStartMills = rebuyStartMills;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public static void setLOG(Logger LOG) {
        TableStatus.LOG = LOG;
    }

    public String getRestMills() {
        return restMills;
    }

    public void setRestMills(String restMills) {
        this.restMills = restMills;
    }

    public String getStartRestTimeMills() {
        return startRestTimeMills;
    }

    public void setStartRestTimeMills(String startRestTimeMills) {
        this.startRestTimeMills = startRestTimeMills;
    }

    public String getUserAllinSeq() {
        return userAllinSeq;
    }

    public void setUserAllinSeq(String userAllinSeq) {
        this.userAllinSeq = userAllinSeq;
    }

    public String getUserStartLeftMoney() {
        return userStartLeftMoney;
    }

    public void setUserStartLeftMoney(String userStartLeftMoney) {
        this.userStartLeftMoney = userStartLeftMoney;
    }

    public String getTableIndex() {
        return tableIndex;
    }

    public void setTableIndex(String tableIndex) {
        this.tableIndex = tableIndex;
    }
}
