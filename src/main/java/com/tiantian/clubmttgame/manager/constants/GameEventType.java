package com.tiantian.clubmttgame.manager.constants;

/**
 *
 */
public interface GameEventType {
    String JOIN_ROOM = "join_room"; // 玩家进入房间
    String EXIT_ROOM = "exit_room";
    String CHANGE_ROOM = "change_room"; // 玩家换房间
    String USERS_INFO = "mtt_users_info"; // 玩家信息
    String USERS_OP = "users_op"; // 其他玩家的操作


    String TABLE_BEGIN = "mtt_table_begin";
    String DANDB = "d_and_b"; // 通知客户端确定庄家,大小盲注
    String PREFLOP = "pre_flop"; // 通知客户端确定庄家,大小盲注
    String FOLD = "fold"; // 通知客户端玩家弃牌
    String CHECK = "check"; // 通知客户端玩家让牌
    String BET = "bet"; // 通知玩家需要下注
    String CALL = "call";
    String ALLIN = "allin";
    String RAISE = "raise"; // 通知玩家下注
    String STAND_UP = "stand_up"; // 通知其他玩家某个玩家站起
    String LEAVE = "leave";
    String ALL_SHOW_CARDS = "all_show_cards";//所有玩家allin后亮牌
    String SHOW_CARDS = "show_cards"; // 玩家亮牌
    String START_COUNT_DOWN = "start_count_down";
    String ADD_TMIES = "add_times"; //
    String ADD_CHIPS = "add_chips"; //
    String SHALOU_NOT_ENOUGH = "shalou_not_enough"; //

    String FLOP_CARDS = "flop_cards"; // 通知玩家三张牌
    String TURN_CARDS = "turn_cards"; // 通知玩家第四张牌
    String RIVER_CARDS = "river_cards"; // 通知玩家第五张牌
    String FINISHED = "finished"; // 通知游戏结束
    String AUTO_CHIPS = "auto_chips"; // 自动带入游戏币
    String CHIPS_NOT_ENOUGH = "chips_not_enough"; // 筹码不足
    String CARD_LEVEL = "card_level"; //通知牌型
    String POOL_INFO = "pool_info"; //通知牌型
    String CAN_START = "can_start"; // 可以开始

    String ERROR_INFO = "error_info"; // 异常信息

    String USER_BUY_IN = "user_buy_in"; // 玩家买入申请通知
    String USER_BUY_IN_OK = "user_buy_in_ok"; // 玩家买入申请通知成功

    String PAUSE_GAME = "pause_game";
    String RESTART_GAME = "restart_game";
    String CLOSE_GAME = "close_game";
    String RANKING = "ranking";

    String BLIND_UP = "mtt_blind_up";
    String CHANGE_TABLE = "change_table";

    String RANKING_UPDATE = "ranking_update";

    String MTT_TABLE_CLOSE = "mtt_table_close";

    String MTT_USER_REBUY = "mtt_user_rebuy";

    String WAIT_REBUY = "mtt_wait_other_rebuy";

    String MTT_HANG = "mtt_hang";

    String MTT_REST = "mtt_rest";

    String MTT_EMOJI = "mtt_emoji";

    String FINAL_TABLE = "mtt_final_table";

    String MTT_USER_RETURN = "mtt_user_return";
}
