package com.tiantian.clubmttgame.utils;

import com.alibaba.fastjson.JSON;
import com.tiantian.clubmttgame.data.redis.RedisUtil;
import com.tiantian.clubmttgame.manager.model.MttGameRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class RecordUtils {
    private static Logger LOG = LoggerFactory.getLogger(RecordUtils.class);
    private static String PRE_KEY = "mtt_record:";

    public static MttGameRecord getLastedRecord(String gameId, String tableId) {
        String key = PRE_KEY + ":" + gameId + ":" + tableId;
        String result = RedisUtil.lindex(key, 0);
        if (result != null) {
            return JSON.parseObject(result, MttGameRecord.class);
        }
        return null;
    }

    public static void addRecord(MttGameRecord mttGameRecord, String gameId, String tableId) {
        if (mttGameRecord == null) {
            return;
        }
        String key = PRE_KEY + ":" + gameId + ":" + tableId;
        RedisUtil.lpush(key, JSON.toJSONString(mttGameRecord));
        RedisUtil.expire(key, 24 * 3600);
    }

    public static void restLastedRecord(MttGameRecord mttGameRecord, String gameId, String tableId) {
        if (mttGameRecord == null) {
            return;
        }
        String key = PRE_KEY + ":" + gameId + ":" + tableId;
        RedisUtil.lset(key,  0l, JSON.toJSONString(mttGameRecord));
        RedisUtil.expire(key, 24 * 3600);
    }
}
