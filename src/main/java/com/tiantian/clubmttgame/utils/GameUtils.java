package com.tiantian.clubmttgame.utils;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmtt.akka.event.user.GameUserFoldEvent;
import com.tiantian.clubmttgame.akka.event.GameTask;
import com.tiantian.clubmttgame.akka.event.TableTaskEvent;
import com.tiantian.clubmttgame.data.redis.RedisUtil;
import com.tiantian.clubmttgame.handlers.Handlers;
import com.tiantian.clubmttgame.manager.constants.GameConstants;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.constants.GameStatus;
import com.tiantian.clubmttgame.manager.model.*;
import com.tiantian.clubmttgame.manager.texas.PokerManager;
import com.tiantian.clubmttgame.manager.texas.PokerOuts;
import com.tiantian.system.proxy_client.NotifierIface;
import com.tiantian.system.thrift.notifier.NoticeType;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 */
public class GameUtils {
    static Logger LOG = LoggerFactory.getLogger(GameUtils.class);

    public static void pushTask(UntypedActorContext context, TableTaskEvent tableTaskEvent, long delayMill) {
        //System.out.println("pushTask====" + JSON.toJSONString(tableTaskEvent));
        context.parent().tell(new GameTask(tableTaskEvent, delayMill), ActorRef.noSender());
    }

    /**
     * 获取除一次大小盲注下注的 开始下注座位号,每次都从小盲注开始，如果小盲注弃牌则按顺序下一个
     * @return
     */
    public static String getPerBeginBetSitNum(Set<String> canBetSitNumSets, String smallBlindSitNum,
                                              String bigBlindSitNum, boolean onlyTwoJoin) {
        if (onlyTwoJoin) { // 每次都从大盲注开叫
            return bigBlindSitNum;
        }
        TreeSet<String> treeSet = new TreeSet<>(canBetSitNumSets);
        for(String sitNum : treeSet) {
            if (Integer.parseInt(sitNum) >= Integer.parseInt(smallBlindSitNum)) {
                return sitNum;
            }
        }
        if (treeSet.size() > 0) {
            // 没有找到则第一个
            return treeSet.first();
        }
        return null;
    }

    // 获取下一个下注的座位号
    public static String getNextBetSitNum(Set<String> sitNumSets, String currentBetSitNum, String maxBetSitNum) {
      //  LOG.info("getNextBetSitNum:sitNumSets"+JSON.toJSONString(sitNumSets) + ";currentBetSitNum:" + currentBetSitNum +";maxBetSitNum:" +maxBetSitNum);
        // 如果能下注的座位列表不包括最大下注，则加入列表中。最大下注座位有可能退出了房间
        boolean maxBetIsExit = false; // 最大下注是否已经退出房间
        if (maxBetSitNum != null && !sitNumSets.contains(maxBetSitNum)) {
            sitNumSets.add(maxBetSitNum);
            maxBetIsExit = true;
        }
        TreeSet<String> treeSet = new TreeSet<>(sitNumSets);
        for (String sitNum : treeSet) {
            if (StringUtils.isNotBlank(currentBetSitNum)
                    && Integer.parseInt(sitNum) > Integer.parseInt(currentBetSitNum)) {
                // 下一个玩家是最大下注者，并且这个最大下注者已经退出房间，则没有需要下注的
                if(sitNum.equalsIgnoreCase(maxBetSitNum) && maxBetIsExit) {
                   return null;
                }
                return sitNum;
            }
        }
        if (treeSet.size() > 0) {
            // 没有找到则第一个
            String sitNum = treeSet.first();
            if (sitNum.equalsIgnoreCase(currentBetSitNum)) {
                return null;
            }
            // 下一个玩家是最大下注者，并且这个最大下注者已经退出房间，则没有需要下注的
            if(sitNum.equalsIgnoreCase(maxBetSitNum) && maxBetIsExit) {
                return null;
            }
            return sitNum;
        }
        return null;
    }

    // 触发检测下注任务
    public static void triggerBetTestTask(UntypedActorContext context,
                                          String tableId, String inningId,
                                          String sitNum, String betUserId,
                                          int operateCount, String pwd,
                                          boolean isExit, long userBetSecs,
                                          String gameId) {
        JSONObject object = new JSONObject();
        object.put(GameConstants.TASK_EVENT, GameConstants.TEST_BET);
        object.put("tableId", tableId);
        object.put("inningId", inningId);
        object.put("sitNum", sitNum);
        object.put("userId", betUserId);
        object.put("operateCount", operateCount + "");
        object.put("pwd", pwd);
        object.put("gameId", gameId);
        if (userBetSecs == GameConstants.BET_DELAYER_TIME / 1000) {
            userBetSecs += 2; // 延迟2s 下注
        }
        pushTask(context, new TableTaskEvent(GameConstants.TEST_BET, object), isExit ? 1000 : userBetSecs * 1000);
    }


    // 触发检测下个任务
    public static void triggerNextStatusTask(GameStatus nextStatus,
                                             String tableId,
                                             String inningId,
                                             UntypedActorContext context,
                                             String gameId) {
        JSONObject object = new JSONObject();
        object.put(GameConstants.TASK_EVENT, GameConstants.TEST_STATUS);
        object.put("tableId", tableId);
        object.put("inningId", inningId);
        object.put("gameId", gameId);
        object.put("selector", nextStatus.name());
        long delay = GameConstants.STATUS_DELAYER_TIME;
        if (nextStatus.name().equalsIgnoreCase(GameStatus.FINISH.name())) {
            delay = 0;
        }
        pushTask(context, new TableTaskEvent(GameConstants.TEST_STATUS, object), delay);
    }

    public static void userRebuyTask(String tableId,
                                     String inningId,
                                     UntypedActorContext context,
                                     String gameId, List<String> overList,
                                     Map<String, Long> userLeftChips) {
        JSONObject object = new JSONObject();
        object.put(GameConstants.TASK_EVENT, GameConstants.USER_REBUY_TASK);
        object.put("tableId", tableId);
        object.put("inningId", inningId);
        object.put("gameId", gameId);
        object.put("overList", overList);
        object.put("userLeftChips", userLeftChips);
        pushTask(context, new TableTaskEvent(GameConstants.USER_REBUY_TASK, object), GameConstants.USER_REBUY_TIME);
    }

    // 发送消息给玩家
    public static void notifyUsers(JSONObject userData, String event,  Collection<String> toUserIds, String id, String tableId) {
        try {
            if (toUserIds == null) {
                return;
            }
            if (userData != null) {
                // 添加游戏消息来源
                userData.put(GameConstants.MSG_SOURCE, "mtt_scene");
                if (StringUtils.isNotBlank(tableId)) {
                    userData.put(GameConstants.UNIQUE_ID, tableId);
                }
            }
//            LOG.info("notifyUsers: notify toUserIds" + JSON.toJSONString(toUserIds));
//            LOG.info("notifyUsers: notify userData" + userData.toJSONString());
            String type = NoticeType.GAME.toString();
            String data = NotifierDataUtils.generate(id, type, userData.toJSONString());
            // 传输数据
            Map<String, String> dataMap = new HashMap<>();
            dataMap.put(GameConstants.DATA_STR, data);
            dataMap.put(GameConstants.EVENT, event);
            for (String toUserId : toUserIds) {
                String serverId = RedisUtil.get(GameConstants.ROUTER_KEY + toUserId);
                if (StringUtils.isBlank(serverId)) {
                    continue;
                }
                // 分发到指定的服务器上
                dataMap.put(GameConstants.SERVER_ID, serverId);
                dataMap.put(GameConstants.TO, toUserId);
                try {
                    // 分发到game的队列中
                    NotifierIface.instance().iface().notify(NoticeType.GAME.getValue(), "MttGameService", dataMap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void notifyUser(JSONObject userData, String event, String toUserId, String tableId) {
        try {
            if (userData != null) {
                // 添加游戏消息来源
                userData.put(GameConstants.MSG_SOURCE, "mtt_scene");
                if (StringUtils.isNotBlank(tableId)) {
                    userData.put(GameConstants.UNIQUE_ID, tableId);
                }
            }

//            LOG.info("notifyUser: notify toUserId" + toUserId);
//            LOG.info("notifyUser: notify userData" + userData.toJSONString());
            String id = UUID.randomUUID().toString().replace("-", "");
            String type = NoticeType.GAME.toString();
            String data = NotifierDataUtils.generate(id, type, userData.toJSONString());
            if (data == null) {
                return;
            }
            // 传输数据
            Map<String, String> dataMap = new HashMap<>();
            dataMap.put(GameConstants.DATA_STR, data);
            dataMap.put(GameConstants.EVENT, event);

            String serverId = RedisUtil.get(GameConstants.ROUTER_KEY + toUserId);
            if (StringUtils.isBlank(serverId)) {
                return;
            }
            // 分发到指定的服务器上
            dataMap.put(GameConstants.SERVER_ID, serverId);
            dataMap.put(GameConstants.TO, toUserId);
            // 分发到game的队列中
            NotifierIface.instance().iface().notify(NoticeType.GAME.getValue(), "MttGameService", dataMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static GameStatus nextGameStatus(String currentStatus) {
        return GameStatus.next(currentStatus);
    }

    public static GameStatus checkNextStatus(TableStatus tableStatus,
                                             String currentStatus,
                                             String maxBetSitNum,
                                             String currentBet) {

        String nextBetSit = getNextBetSitNum(tableStatus.canBetSits(), currentBet, maxBetSitNum);
        // 先判断pre-flop阶段
        if(GameStatus.PRE_FLOP.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return preFlopStatus(tableStatus, currentStatus, maxBetSitNum, currentBet, nextBetSit);
        }
        // 直接结束结算
        if (tableStatus.onlyOneNotFold()) {
            return GameStatus.FINISH;
        }

        // 全部弃牌
        if (tableStatus.allFold()) {
            return GameStatus.END;
        }

        // 直接进入下一个状态
        if (tableStatus.allAllin() || tableStatus.onlyOneNotAllin() || tableStatus.allFoldOrAllin() ||
                tableStatus.checkIsLastBetAndOnlyOneAllin(nextBetSit)) {
            return nextGameStatus(currentStatus);
        }

        // 没有人需要下注，allin和弃牌
        if (nextBetSit == null || nextBetSit.equalsIgnoreCase(maxBetSitNum)) {
            return nextGameStatus(tableStatus.getStatus());
        }
        return null;
    }

    private static GameStatus preFlopStatus(TableStatus tableStatus,
                                     String currentStatus,
                                     String maxBetSitNum,
                                     String currentBet,
                                     String nextBetSit) {
        // 直接结束结算
        if (tableStatus.onlyOneNotFold()) {
            return GameStatus.FINISH;
        }
        // 全部弃牌
        if (tableStatus.allFold()) {
            return GameStatus.END;
        }

        // 直接进入下一个状态
        if (tableStatus.allAllin() || tableStatus.allFoldOrAllin() ||
                tableStatus.checkIsLastBetAndOnlyOneAllin(nextBetSit)) {
            return nextGameStatus(currentStatus);
        }


        // 判断下一个是不是大盲注位置
        if (nextBetSit != null && nextBetSit.equalsIgnoreCase(tableStatus.getBigBlindNum())) {
            // 大盲注没有操作过
            UserBetLog userBetLog = tableStatus.getUserBetLog(nextBetSit);
            if (userBetLog != null) {
                long totalChips =  userBetLog.getRoundChips() + userBetLog.getTotalChips();
                if(totalChips <= Long.parseLong(tableStatus.getBigBlindMoney())) {
                    return null;
                }
            } else {
                return null;
            }
        }
        // 判断当前下注的是不是大盲注位
        if(currentBet != null && currentBet.equalsIgnoreCase(tableStatus.getBigBlindNum())) {
            // 判断当前最大下注是不是一个大盲注,表示大盲注位第一次跟注
            if (tableStatus.getMaxBetSitNumChips().equalsIgnoreCase(tableStatus.getBigBlindMoney())) {
                return nextGameStatus(currentStatus);
            }
        }

        if (tableStatus.onlyOneNotAllin()) {
            return nextGameStatus(currentStatus);
        }
        // 没有人需要下注，allin和弃牌
        if (nextBetSit == null || nextBetSit.equalsIgnoreCase(maxBetSitNum)) {
            return nextGameStatus(tableStatus.getStatus());
        }
        return null;
    }


    public static GameStatus beforeCheckNextStatus(String currentStatus,
                                                   TableStatus tableStatus) {
        // 直接进入下一个状态
        if (tableStatus.allAllin() || tableStatus.onlyOneNotAllin()) {
            return nextGameStatus(currentStatus);
        }
        // 直接结束结算
        if (tableStatus.onlyOneNotFold()) {
            return GameStatus.FINISH;
        }
        // 全部弃牌
        if (tableStatus.allFold()) {
            return GameStatus.END;
        }
        return null;
    }

    public static void nextStatusTask(GameStatus nextStatus, String tableId, String inningId,
                                      ActorRef self, UntypedActorContext context, ActorRef sender) {
        if (nextStatus != null) {
            // 立即触发下一个
            JSONObject eventParam = new JSONObject();
            eventParam.put("tableId", tableId);
            eventParam.put("inningId", inningId);
            TableTaskEvent event = new TableTaskEvent();
            event.setEvent(nextStatus.name());
            event.setParams(eventParam);
            Handlers.INSTANCE.execute(event, self, context, sender);
        }
    }

    // 通知下一个玩家下注
    public static void noticeNextUserBet(String tableId, TableStatus tableStatus,
                                         String maxBetSitNum, boolean isPerBegin,
                                         String maxBetSitNumChips, ActorRef self, UntypedActorContext context,
                                         ActorRef sender) {
        Set<String> canBetSets = tableStatus.canBetSits();
        String smallBlindNum = tableStatus.getSmallBlindNum();
        String beginBetSitNum = "";
        MttGameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getRoomId(), tableStatus.getTableId());
        if (mttGameRecord == null) {
            mttGameRecord = new MttGameRecord();
        }
        List<MttGameRecord.Progress> progresses = mttGameRecord.getProgresses();

        // 判断是否是每轮的开始
        if (isPerBegin) {
            beginBetSitNum = getPerBeginBetSitNum(canBetSets, smallBlindNum,
                    tableStatus.getBigBlindNum(), tableStatus.getUsersCards().size() == 2);
            if (StringUtils.isNotBlank(beginBetSitNum)) {
                tableStatus.setMaxBetSitNum(beginBetSitNum);
                tableStatus.setMaxBetSitNumChips("0");
            } else {
                // 直接进入下个发牌
                GameStatus nextStatus = checkNextStatus(tableStatus, tableStatus.getStatus(), maxBetSitNum, null);
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    // 保存数据
                    tableStatus.save();
                    // 触发下一轮的发牌事件
                    GameUtils.nextStatusTask(nextStatus, tableId, tableStatus.getInningId(), self, context, sender);
                    return;
                } else {
                    throw new RuntimeException("can not found next GameStatus");
                }
            }
        }
        else {
            beginBetSitNum = getNextBetSitNum(canBetSets, tableStatus.getCurrentBet(), maxBetSitNum);
            if (StringUtils.isBlank(beginBetSitNum)) {
                GameStatus nextStatus = checkNextStatus(tableStatus, tableStatus.getStatus(), maxBetSitNum, null);
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    // 保存数据
                    tableStatus.save();
                    // 触发下一轮的发牌事件
                    GameUtils.nextStatusTask(nextStatus, tableId, tableStatus.getInningId(), self, context, sender);
                    return;
                } else {
                    throw new RuntimeException("can not found next GameStatus");
                }
            }
            if (StringUtils.isNotBlank(maxBetSitNum)) {
                // 当最大下注的人座位号不为空则直接设置
                tableStatus.setMaxBetSitNum(maxBetSitNum);
            }
            if (StringUtils.isNotBlank(maxBetSitNumChips)) {
                // 设置最大下注
                tableStatus.setMaxBetSitNumChips(maxBetSitNumChips);
            }
        }
        // 设置当前需要下注人座位号
        tableStatus.setCurrentBet(beginBetSitNum);
        // 设置当前需要下注人座位号开始时间
        tableStatus.setCurrentBetTimes( System.currentTimeMillis() + "");
        String indexCount = tableStatus.getIndexCount();
        String pwd = tableStatus.randomPwd();
        // 刷新修改的
        tableStatus.save();


        progresses.add(MttGameRecord.Progress.create("bet", beginBetSitNum, "nil",
                System.currentTimeMillis() - mttGameRecord.getStartTime()));
        RecordUtils.restLastedRecord(mttGameRecord, tableStatus.getRoomId(), tableId);


        TableAllUser tableAllUser = TableAllUser.load(tableId);
        //通知下注的玩家需要下注
        String betUserId = tableAllUser.userSitDownUsers().get(beginBetSitNum);
        TableUser betTableUser = TableUser.load(betUserId, tableId);
        if (betTableUser.isHang()) { // 如果下注的是挂机 直接弃牌
            GameUserFoldEvent gameUserFoldEvent = new GameUserFoldEvent(betUserId, pwd, tableStatus.getRoomId());
            gameUserFoldEvent.setTbId(tableId);
            Handlers.INSTANCE.execute(gameUserFoldEvent, self, context, sender);
            return;
        }

        if(tableAllUser.isOnline(betUserId)) {
            JSONObject userData = new JSONObject();
            userData.put("inner_id", tableStatus.getInningId());
            userData.put("inner_cnt", indexCount);
            userData.put("uid", betUserId);
            userData.put("sn", Integer.parseInt(beginBetSitNum));
            userData.put("t", Long.parseLong(betTableUser.getTotalSecs()));

            String[] ops = tableStatus.getUserCanOps(betUserId, beginBetSitNum);
            userData.put("c_b", StringUtils.join(ops, ","));
            userData.put("pwd", pwd);

            // 通知下注的玩家进行下注
            notifyUser(userData, GameEventType.BET, betUserId, tableStatus.getRoomId());
        }


        // 通知其他玩家下注玩家需要下注
        JSONObject otherNextObject = new JSONObject();
        otherNextObject.put("inner_id", tableStatus.getInningId());
        otherNextObject.put("inner_cnt", indexCount);
        otherNextObject.put("uid", betUserId);
        otherNextObject.put("sn", Integer.parseInt(beginBetSitNum));
        otherNextObject.put("t", Long.parseLong(betTableUser.getTotalSecs()));
        otherNextObject.put("c_b", "");
        otherNextObject.put("pwd", "");
        Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
        userIds.remove(betUserId);

        String newId = UUID.randomUUID().toString().replace("-", "");
        notifyUsers(otherNextObject, GameEventType.BET, userIds, newId, tableStatus.getRoomId());


        if(StringUtils.isBlank(betTableUser.getOperateCount())) {
           betTableUser.setOperateCount("1");
           betTableUser.save();
        }
        // 触发30s的检测任务
        triggerBetTestTask(context, tableId, tableStatus.getInningId(), beginBetSitNum, betUserId,
                Integer.parseInt(betTableUser.getOperateCount()), pwd, betTableUser.isExit(),
                Long.parseLong(betTableUser.getTotalSecs()), tableStatus.getRoomId());
    }

    // 通知每个玩家的牌行
    public static void noticeCardsLevel(TableStatus tableStatus, TableAllUser tableAllUser) {
        Map<String, String> sitCardsMap = tableStatus.allSitNumsAndCards();
        Map<String, PokerOuts> pokerOutsMap = PokerManager.getUsersPokerOutsList(sitCardsMap,
                                                                                         tableStatus.getDeskCardList());
        Set<Map.Entry<String, PokerOuts>> mapEntrySet = pokerOutsMap.entrySet();
        for (Map.Entry<String, PokerOuts> entry : mapEntrySet) {
             String sitNum = entry.getKey();
             if (tableAllUser.getGamingSitUserMap() == null) {
                 continue;
             }
             String userId = tableAllUser.userSitDownUsers().get(sitNum);
             if (StringUtils.isBlank(userId)) {
                 continue;
             }
             PokerOuts pokerOuts = entry.getValue();
             JSONObject userData = new JSONObject();
             userData.put("inner_id", tableStatus.getInningId());
             userData.put("inner_cnt", tableStatus.getIndexCount());
             userData.put("level", pokerOuts.getLevel());
             if (tableAllUser.isOnline(userId)) {
                 notifyUser(userData, GameEventType.CARD_LEVEL, userId, tableStatus.getRoomId());
             }
        }
    }

    public static void notifyUserPoolInfo(TableStatus tableStatus, TableAllUser tableAllUser) {
        //分池信息没有变化则不发送分池信息
        if (tableStatus.getLastBetPool() != null &&
                tableStatus.getLastBetPool().equalsIgnoreCase(tableStatus.getBetPool())) {
            return;
        }
        List<Long> poolList = tableStatus.betPoolList();
        JSONObject $object = new JSONObject();
        $object.put("inner_id", tableStatus.getInningId());
        $object.put("inner_cnt", tableStatus.getIndexCount());
        $object.put("pool_info", StringUtils.join(poolList, ","));
        String id = UUID.randomUUID().toString().replace("-", "");
        notifyUsers($object, GameEventType.POOL_INFO, tableAllUser.getOnlineTableUserIds(), id, tableStatus.getRoomId());
    }

    public static void tableStatusTask(UntypedActorContext context, String inningId ,String tableId, String gameId) {
        JSONObject object = new JSONObject();
        object.put(GameConstants.TASK_EVENT, GameConstants.TABLE_STATUS_CHECK);
        object.put("tableId", tableId);
        object.put("inningId", inningId);
        object.put("gameId", gameId);
        pushTask(context, new TableTaskEvent(GameConstants.TABLE_STATUS_CHECK, object), GameConstants.TABLE_STATUS_DELAYER_TIME);

    }

    public static Map<String, Long> rankingUserChips(Map<String, Long> userLeftChips, Map<String, Long> userBeginChips,
                                               List<String> userAllinSeq) {
        Map<String, Long> result = new LinkedHashMap<>();
        List<UserChipsRanking> rankings = new ArrayList<>();
        for (Map.Entry<String, Long> entry : userLeftChips.entrySet()) {
            String userId = entry.getKey();
            Long leftChips = entry.getValue();
            UserChipsRanking userChipsRanking = new UserChipsRanking();
            userChipsRanking.setUserId(userId);
            userChipsRanking.setLeftChips(leftChips);
            Long beginChips = userBeginChips.get(userId);
            if (beginChips == null) {
                beginChips = 0l;
            }
            userChipsRanking.setBeginChips(beginChips);
            if (leftChips == 0) { // 剩余筹码为0
                int allIndex = userAllinSeq.indexOf(userId) + 1;
                userChipsRanking.setAllinIndex(allIndex);
            } else {
                userChipsRanking.setAllinIndex(0);
            }
            rankings.add(userChipsRanking);
        }
        Collections.sort(rankings, (o1, o2) -> {
            int r1 = o2.getLeftChips().compareTo(o1.getLeftChips());
            if (r1 != 0) {
                return r1;
            }
            if (o2.getLeftChips() == 0) {
                int r2 = o2.getBeginChips().compareTo(o1.getBeginChips());
                if (r2 != 0) {
                    return r2;
                }
                return o1.getAllinIndex().compareTo(o2.getAllinIndex());
            }
            return r1;
        });

        for (UserChipsRanking userChipsRanking : rankings) {
            String userId = userChipsRanking.getUserId();
            Long chips = userChipsRanking.getLeftChips();
            result.put(userId, chips);
        }
        return result;
    }

}
