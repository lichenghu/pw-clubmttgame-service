package com.tiantian.clubmttgame.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.sharding.ClusterSharding;
import akka.cluster.sharding.ClusterShardingSettings;
import akka.cluster.sharding.ShardRegion;
import com.tiantian.clubmtt.akka.event.Event;
import com.tiantian.clubmtt.akka.event.GameEvent;
/**
 *
 */
public class ShardingManager extends UntypedActor {
    private String port;
    private ActorRef shardRegion;
    private Cluster cluster;

    public ShardingManager(String port) {
        this.port = port;
    }

    public void preStart () throws Exception {
        super.preStart();
        cluster = Cluster.get(getContext().system());
        cluster.subscribe(self(), ClusterEvent.MemberEvent.class);
        ClusterShardingSettings settings = ClusterShardingSettings.create(getContext().system());
        this.shardRegion = ClusterSharding.get(getContext().system()).start("ClubMttManagerActor",
                Props.create(MttManagerActor.class), settings, messageExtractor);
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Event) {
             shardRegion.forward(message, getContext());
         }
         else {
            unhandled(message);
         }
    }


    private static ShardRegion.HashCodeMessageExtractor messageExtractor = new ShardRegion.HashCodeMessageExtractor(20) {
        @Override
        public String entityId(Object o) {
             if(o instanceof GameEvent) {
                return ((GameEvent) o).gameId();
             }
             return null;
        }
    };
}
