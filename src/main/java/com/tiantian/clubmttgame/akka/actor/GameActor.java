package com.tiantian.clubmttgame.akka.actor;

import akka.actor.UntypedActor;
import com.alibaba.fastjson.JSON;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.mail.proxy_client.MailIface;
import com.tiantian.clubmttgame.akka.event.*;
import com.tiantian.clubmttgame.data.mongodb.MGDatabase;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.SimpleDateFormat;
import java.util.List;
/**
 *
 */
public class GameActor extends UntypedActor {
    private static Logger LOG = LoggerFactory.getLogger(GameActor.class);
    private static final String MTT_GAME = "mtt_rooms";
    private static final String MTT_TABLE_USER = "mtt_rooms_users";
    private String gameId;

    public GameActor(String gameId) {
           this.gameId = gameId;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        LOG.info("GameActor:" + JSON.toJSONString(message));
        if (message instanceof LocalGameOver) {
            updateMttOver();
            long ret = updateGameOver(((LocalGameOver) message).gameId());
            LOG.info("updateGameOver counts:" + ret);
        } else if (message instanceof LocalUpdateUserStatusEvent) {
            long ret = updateUserStatus(((LocalUpdateUserStatusEvent) message).getUserIds());
            LOG.info("updateUserStatus counts:" + ret);
        }
        else if (message instanceof LocalUpdateChips) {
            boolean ret = updateLeftChips(((LocalUpdateChips) message).getUserId(),
                    ((LocalUpdateChips) message).getLeftChips(), ((LocalUpdateChips) message).getRanking());
            LOG.info("updateLeftChips ret:" + ret);
        } else if (message instanceof LocalUpdateBlindEvent) {
            updateBlind(((LocalUpdateBlindEvent) message).getCurrentBlindLvl(), ((LocalUpdateBlindEvent) message).getNextUpgradeTime());
        } else if (message instanceof LocalUserRebuySuccessEvent) {
            updateUserRebuy(((LocalUserRebuySuccessEvent) message).getBuyChips(),
                    ((LocalUserRebuySuccessEvent) message).getUserId());
        }
        else {
            unhandled(message);
        }
    }


    private long updateUserStatus(List<String> userIds) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
        long i = 0;
        for (String userId : userIds) {
             BasicDBObject selectCondition = new BasicDBObject();
             selectCondition.put("gameId",  gameId);
             selectCondition.put("userId",  userId);
             BasicDBObject updatedValue = new BasicDBObject();
             updatedValue.put("status", "gaming");
             BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
             UpdateResult result = collection.updateMany(selectCondition, updateSetValue);
             long cnt = result.getModifiedCount();
             i += cnt;
        }
        return i;
    }

    private long updateGameOver(String gameId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("gameId",  gameId);
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("status", "end");
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = collection.updateMany(selectCondition, updateSetValue);
        return result.getModifiedCount();
    }

    private boolean updateLeftChips(String userId, long leftChips, int ranking) {
        LOG.info("updateLeftChips:" + userId + ",leftChips:" + leftChips, ",gameId:" + gameId);
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("gameId",  gameId);
        selectCondition.put("userId",  userId);
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("leftChips", leftChips);
        updatedValue.put("ranking", ranking);
        String status = "gaming";
        if (leftChips == 0) {
            status = "end";
        }
        updatedValue.put("status", status);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = collection.updateMany(selectCondition, updateSetValue);
        LOG.info("updateLeftChips MatchedCount:" + result.getMatchedCount());
        LOG.info("updateLeftChips ModifiedCount:" + result.getModifiedCount());
        return (result.getModifiedCount() > 0);
    }

    private boolean updateMttUserRanking(String userId, int ranking, long leftChips) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("gameId", gameId);
        selectCondition.put("userId", userId);
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("ranking", ranking);
        updatedValue.put("leftChips", leftChips);
        updatedValue.put("status", "end");
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = collection.updateMany(selectCondition, updateSetValue);
        LOG.info("updateMttUserRanking MatchedCount:" + result.getMatchedCount());
        LOG.info("updateMttUserRanking ModifiedCount:" + result.getModifiedCount());
        return (result.getModifiedCount() > 0);
    }

    private void updateMttOver() {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(gameId));
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("status", -1);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        collection.updateOne(updateCondition, updateSetValue);
    }

    private void updateBlind(int curretBlindLvl, long nextUpgradeTime) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_GAME);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(gameId));
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("curretBlindLvl", curretBlindLvl);
        updatedValue.put("updateBlindTimes", nextUpgradeTime);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        collection.updateOne(updateCondition, updateSetValue);
    }

    private void updateUserRebuy(long addChips, String userId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(MTT_TABLE_USER);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("gameId", gameId);
        updateCondition.put("userId", userId);
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("leftChips", addChips);
        updatedValue.put("rebuyCnt", 1);
        BasicDBObject updateSetValue = new BasicDBObject("$inc", updatedValue);
        UpdateResult updateResult = collection.updateOne(updateCondition, updateSetValue);
        LOG.info("updateUserRebuy:" + updateResult.getModifiedCount());
    }

    private void sendMail(String userId, int ranking, String gameName, long gameStartTime) {
        //  发送邮件
        try {
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            MailIface.instance().iface().saveNotice(userId,
                    "你在日期"+ sf.format(gameStartTime) + "的" + gameName + "比赛中获得了" + ranking + "名");
        } catch (TException e) {
            e.printStackTrace();
        }
    }

}
