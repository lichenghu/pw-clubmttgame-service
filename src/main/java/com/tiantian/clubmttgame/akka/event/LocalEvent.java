package com.tiantian.clubmttgame.akka.event;
import com.tiantian.clubmtt.akka.event.Event;

/**
 *
 */
public interface LocalEvent extends Event {
    String gameId();
}
