package com.tiantian.clubmttgame.akka.event;

/**
 *
 */
public class LocalGameOver extends LocalTableEvent {
    private String gameId;

    public LocalGameOver(String gameId) {
        this.gameId = gameId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    @Override
    public String tableId() {
        return null;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return null;
    }
}
