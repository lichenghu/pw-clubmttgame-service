package com.tiantian.clubmttgame.akka.event;

/**
 *
 */
public class LocalUpdateBlindEvent implements LocalEvent {
    private String gameId;
    private int currentBlindLvl;
    private long nextUpgradeTime;

    public LocalUpdateBlindEvent(String gameId, int currentBlindLvl, long nextUpgradeTime) {
        this.gameId = gameId;
        this.currentBlindLvl = currentBlindLvl;
        this.nextUpgradeTime = nextUpgradeTime;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getCurrentBlindLvl() {
        return currentBlindLvl;
    }

    public void setCurrentBlindLvl(int currentBlindLvl) {
        this.currentBlindLvl = currentBlindLvl;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return null;
    }

    public long getNextUpgradeTime() {
        return nextUpgradeTime;
    }

    public void setNextUpgradeTime(long nextUpgradeTime) {
        this.nextUpgradeTime = nextUpgradeTime;
    }
}
