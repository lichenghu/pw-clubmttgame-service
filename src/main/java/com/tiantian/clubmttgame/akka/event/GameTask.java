package com.tiantian.clubmttgame.akka.event;

import java.io.Serializable;

/**
 *
 */
public class GameTask implements Serializable {
    private TableTaskEvent tableTaskEvent;
    private long delayMill;

    public GameTask(TableTaskEvent tableTaskEvent, long delayMill) {
        this.tableTaskEvent = tableTaskEvent;
        this.delayMill = delayMill;
    }

    public TableTaskEvent getTableTaskEvent() {
        return tableTaskEvent;
    }

    public void setTableTaskEvent(TableTaskEvent tableTaskEvent) {
        this.tableTaskEvent = tableTaskEvent;
    }

    public long getDelayMill() {
        return delayMill;
    }

    public void setDelayMill(long delayMill) {
        this.delayMill = delayMill;
    }
}
