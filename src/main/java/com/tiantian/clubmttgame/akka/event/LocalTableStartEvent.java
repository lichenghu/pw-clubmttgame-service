package com.tiantian.clubmttgame.akka.event;

import com.tiantian.clubmtt.akka.event.game.GameStartEvent;

import java.util.List;

/**
 *
 */
public class LocalTableStartEvent extends LocalTableEvent {
    private String tableId;
    private String gameId;
    private boolean canStart;
    private List<String> userIds;
    private int totalUsers;
    private long averageChips;
    private GameStartEvent.Game game;
    private List<GameStartEvent.UserInfo> userInfos;
    private long netxUpBlindTimes;
    private int rebuyCnt;
    private int tableIndex;

    public LocalTableStartEvent(String tableId, String gameId, boolean canStart, List<String> userIds,
                                GameStartEvent.Game game, List<GameStartEvent.UserInfo> userInfos,
                                int totalUsers, long averageChips, long netxUpBlindTimes, int tableIndex) {
        this.tableId = tableId;
        this.gameId = gameId;
        this.canStart = canStart;
        this.userIds = userIds;
        this.game = game;
        this.userInfos = userInfos;
        this.totalUsers = totalUsers;
        this.averageChips = averageChips;
        this.netxUpBlindTimes = netxUpBlindTimes;
        this.tableIndex = tableIndex;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "tableStart";
    }

    public GameStartEvent.Game getGame() {
        return game;
    }

    public void setGame(GameStartEvent.Game game) {
        this.game = game;
    }

    public List<GameStartEvent.UserInfo> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<GameStartEvent.UserInfo> userInfos) {
        this.userInfos = userInfos;
    }

    public boolean isCanStart() {
        return canStart;
    }

    public void setCanStart(boolean canStart) {
        this.canStart = canStart;
    }

    public int getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(int totalUsers) {
        this.totalUsers = totalUsers;
    }

    public long getAverageChips() {
        return averageChips;
    }

    public void setAverageChips(long averageChips) {
        this.averageChips = averageChips;
    }

    public long getNetxUpBlindTimes() {
        return netxUpBlindTimes;
    }

    public void setNetxUpBlindTimes(long netxUpBlindTimes) {
        this.netxUpBlindTimes = netxUpBlindTimes;
    }

    public int getRebuyCnt() {
        return rebuyCnt;
    }

    public void setRebuyCnt(int rebuyCnt) {
        this.rebuyCnt = rebuyCnt;
    }

    public int getTableIndex() {
        return tableIndex;
    }

    public void setTableIndex(int tableIndex) {
        this.tableIndex = tableIndex;
    }
}
