package com.tiantian.clubmttgame.akka.event;

/**
 *
 */
public class LocalGameEvent implements LocalEvent{

    private String gameId;
    private String event;
    private long delay;

    public LocalGameEvent(String gameId, String event, long delay) {
        this.gameId = gameId;
        this.event = event;
        this.delay = delay;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return event;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }
}
