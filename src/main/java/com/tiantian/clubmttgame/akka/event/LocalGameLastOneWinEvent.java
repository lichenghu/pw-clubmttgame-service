package com.tiantian.clubmttgame.akka.event;

/**
 *
 */
public class LocalGameLastOneWinEvent  extends LocalTableEvent {
    private String tableId;
    private String gameId;
    private String userId;

    public LocalGameLastOneWinEvent(String tableId, String gameId, String userId) {
        this.tableId = tableId;
        this.gameId = gameId;
        this.userId = userId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "lastOneWin";
    }

    public String getUserId() {
        return userId;
    }
}
