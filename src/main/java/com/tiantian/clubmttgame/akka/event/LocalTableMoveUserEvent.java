package com.tiantian.clubmttgame.akka.event;

import java.util.List;

/**
 *
 */
public class LocalTableMoveUserEvent extends LocalTableEvent {
    private String tableId;
    private String gameId;
    private List<String> userIds;

    public LocalTableMoveUserEvent()  {}

    public LocalTableMoveUserEvent(String tableId, String gameId, List<String> userIds) {
        this.tableId = tableId;
        this.gameId = gameId;
        this.userIds = userIds;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "moveUser";
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }
}
