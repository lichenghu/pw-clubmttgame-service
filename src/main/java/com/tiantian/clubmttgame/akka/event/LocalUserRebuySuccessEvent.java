package com.tiantian.clubmttgame.akka.event;

/**
 *
 */
public class LocalUserRebuySuccessEvent extends LocalTableEvent {
    private String gameId;
    private String tableId;
    private String userId;
    private long buyChips;

    public LocalUserRebuySuccessEvent() {
    }

    public LocalUserRebuySuccessEvent(String gameId, String tableId, String userId, long buyChips) {
        this.gameId = gameId;
        this.tableId = tableId;
        this.userId = userId;
        this.buyChips = buyChips;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return null;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getBuyChips() {
        return buyChips;
    }

    public void setBuyChips(long buyChips) {
        this.buyChips = buyChips;
    }
}
