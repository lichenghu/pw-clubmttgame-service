package com.tiantian.clubmttgame.akka.event;

/**
 *
 */
public class LocalGameOnlineWatchEvent extends LocalTableEvent {
    private String userId;
    private String tableId;

    public LocalGameOnlineWatchEvent() {
    }

    public LocalGameOnlineWatchEvent(String userId, String tableId) {
        this.userId = userId;
        this.tableId = tableId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return null;
    }

    @Override
    public String event() {
        return "watchOnline";
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
