package com.tiantian.clubmttgame.akka.event;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
public class LocalTableAddUserEvent extends LocalTableEvent {
    private String tableId;
    private String gameId;
    private List<UserInfo> userInfos;
    private long initChips; // 如果大于0 表示延时加入的玩家
    private boolean isFinalTable;
    private int rebuyCnt;// 玩家是否是rebuy进入

    public LocalTableAddUserEvent()  {}

    public LocalTableAddUserEvent(String tableId, String gameId, List<UserInfo> userInfos) {
        this.tableId = tableId;
        this.gameId = gameId;
        this.userInfos = userInfos;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "addUser";
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public List<UserInfo> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }

    public long getInitChips() {
        return initChips;
    }

    public void setInitChips(long initChips) {
        this.initChips = initChips;
    }

    public boolean isFinalTable() {
        return isFinalTable;
    }

    public void setIsFinalTable(boolean isFinalTable) {
        this.isFinalTable = isFinalTable;
    }

    public int getRebuyCnt() {
        return rebuyCnt;
    }

    public void setRebuyCnt(int rebuyCnt) {
        this.rebuyCnt = rebuyCnt;
    }


    public static class UserInfo implements Serializable {
        private String userId;
        private String nickName;
        private String avatarUrl;
        private String gender;
        private String ranking;
        private int buyInCnt;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getRanking() {
            return ranking;
        }

        public void setRanking(String ranking) {
            this.ranking = ranking;
        }

        public int getBuyInCnt() {
            return buyInCnt;
        }

        public void setBuyInCnt(int buyInCnt) {
            this.buyInCnt = buyInCnt;
        }


    }
}
