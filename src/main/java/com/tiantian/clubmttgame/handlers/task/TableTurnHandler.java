package com.tiantian.clubmttgame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmttgame.akka.event.TableTaskEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.constants.GameStatus;
import com.tiantian.clubmttgame.manager.model.MttGameRecord;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import com.tiantian.clubmttgame.manager.texas.Poker;
import com.tiantian.clubmttgame.utils.GameUtils;
import com.tiantian.clubmttgame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
/**
 *
 */
public class TableTurnHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String inningId = jsonObject.getString("inningId");

        //保存第四张牌
        TableStatus tableStatus = TableStatus.load(tableId);
        if(StringUtils.isBlank(tableStatus.getCards())) {
            return;
        }
        if (!GameStatus.FLOP.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return;
        }
        // 已经发过牌了
        if (tableStatus.getDeskCardList() != null && tableStatus.getDeskCardList().size() >= 4) {
            return;
        }
        List<Poker> pokerList = JSON.parseArray(tableStatus.getCards(), Poker.class);
        pokerList.remove(0);
        Poker poker = pokerList.remove(0);
        String turnCards = poker.getShortPoker();
        String deskCardsStr = tableStatus.getDeskCards();

        List<String> deskCards = JSON.parseArray(deskCardsStr, String.class);
        deskCards.add(turnCards);

        tableStatus.setDeskCards(JSON.toJSONString(deskCards));
        tableStatus.setStatus(GameStatus.TURN.name());

        MttGameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getRoomId(), tableStatus.getTableId());
        if (mttGameRecord == null) {
            mttGameRecord = new MttGameRecord();
        }
        List<MttGameRecord.Progress> progresses = mttGameRecord.getProgresses();
        progresses.add(MttGameRecord.Progress.create("turn", "nil", turnCards,
                System.currentTimeMillis() - mttGameRecord.getStartTime()));
        RecordUtils.restLastedRecord(mttGameRecord, tableStatus.getRoomId(), tableId);


        // 发送给玩家
        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("turn_cards", turnCards);

        tableStatus.setCards(JSON.toJSONString(pokerList));
        tableStatus.checkDelay();
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.TURN_CARDS, userIds, id, tableStatus.getRoomId());
        GameUtils.noticeCardsLevel(tableStatus, tableAllUser);
        // 校验是否直接进行下一个发牌
        GameStatus nextStatus = GameUtils.beforeCheckNextStatus(GameStatus.TURN.name(), tableStatus);
        if (nextStatus != null) {
            tableStatus.roundBetEnd();
            tableStatus.save();
            // 通知玩家的池信息
            GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
            // 触发下一轮的发牌事件
            GameUtils.nextStatusTask(nextStatus, tableId, inningId,
                    self, context, sender);
            return;
        }

        GameUtils.noticeNextUserBet(tableId, tableStatus, null, true, null,
                self, context, sender);
    }
}
