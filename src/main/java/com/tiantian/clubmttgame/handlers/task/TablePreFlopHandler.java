package com.tiantian.clubmttgame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmttgame.akka.event.TableTaskEvent;
import com.tiantian.clubmttgame.data.redis.RedisUtil;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameConstants;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.constants.GameStatus;
import com.tiantian.clubmttgame.manager.model.MttGameRecord;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import com.tiantian.clubmttgame.manager.model.TableUser;
import com.tiantian.clubmttgame.manager.texas.Poker;
import com.tiantian.clubmttgame.manager.texas.PokerManager;
import com.tiantian.clubmttgame.utils.GameUtils;
import com.tiantian.clubmttgame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 */
public class TablePreFlopHandler implements EventHandler<TableTaskEvent> {
    private static Logger LOG = LoggerFactory.getLogger(TablePreFlopHandler.class);
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        // 回合ID
        String inningId = jsonObject.getString("inningId");
        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        // 产生当局的牌
        List<Poker> pokerList = PokerManager.initPokers();
        Collection<String> userSits = tableAllUser.userGamingExitHangSitUserId().keySet();
        Map<String, JSONObject> map = new HashMap<>();
        Map<String, String > userCardsMap = new HashMap<>();
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            LOG.error("TableStatus is null");
            return;
        }
        if (!GameStatus.D_AND_B.name().equalsIgnoreCase(tableStatus.getStatus())) {
            LOG.error("last GameStatus is not d_and_b, status is " + tableStatus.getStatus());
            return;
        }
        List<String> exitUserIds = new ArrayList<>();
        MttGameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getRoomId(), tableStatus.getTableId());
        if (mttGameRecord == null) {
            mttGameRecord = new MttGameRecord();
        }
        List<MttGameRecord.Progress> progresses = mttGameRecord.getProgresses();
        progresses.add(MttGameRecord.Progress.create("pre_flop", "nil", "nil",
                System.currentTimeMillis() - mttGameRecord.getStartTime()));
        // 发底牌:每人发一张，发完后再每人发第二张
        for (int i = 0; i <= 1; i++) {
            for (String sitNum : userSits) {
                String userId = tableAllUser.userGamingExitHangSitUserId().get(sitNum);
                if (StringUtils.isBlank(userId)) {
                    continue;
                }
                JSONObject object = map.get(userId);
                Poker poker = pokerList.remove(0);
                String userCards;
                if (object == null) {
                    object = new JSONObject();
                    userCards = poker.getShortPoker();
                    object.put("hand", userCards);
                } else {
                    userCards = (String) object.get("hand");
                    userCards += ("," + poker.getShortPoker());
                    object.put("hand", userCards);
                    progresses.add(MttGameRecord.Progress.create("deal", sitNum, userCards,
                            System.currentTimeMillis() - mttGameRecord.getStartTime()));
                }
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                // 保存玩家的底牌
                userCardsMap.put(sitNum, userCards);
                map.put(userId, object);
            }
        }
        // 保存玩家底牌
        tableStatus.setUserCards(JSON.toJSONString(userCardsMap));
        // 获取当前需要下注的座位号
        String bigBlindSitNum = tableStatus.getBigBlindNum();
        String currentBetSitNum = GameUtils.getNextBetSitNum(tableStatus.canBetSits(), bigBlindSitNum,
                tableStatus.getMaxBetSitNum());
        // 设置当局的牌
        tableStatus.setCards(JSON.toJSONString(pokerList));
        // 设置状态
        tableStatus.setStatus(GameStatus.PRE_FLOP.name());

        if (StringUtils.isBlank(currentBetSitNum)) {
            GameStatus nextStatus = GameUtils.nextGameStatus(GameStatus.PRE_FLOP.name());
            // 保存但并不增加计数
            tableStatus.saveNotAddCnt();
            mttGameRecord.setProgresses(progresses);
            RecordUtils.restLastedRecord(mttGameRecord, tableStatus.getRoomId(), tableId);
            // 触发下一轮的发牌事件
            GameUtils.nextStatusTask(nextStatus, tableId, inningId,  self, context, sender);
            return;
        }
        // 设置当前需要下注人座位号
        tableStatus.setCurrentBet(currentBetSitNum);
        // 设置当前需要下注人座位号开始时间
        tableStatus.setCurrentBetTimes(System.currentTimeMillis() + "");


        // 校验是否直接进行下一个发牌
        GameStatus nextStatus = GameUtils.beforeCheckNextStatus(GameStatus.PRE_FLOP.name(), tableStatus);
        if (nextStatus != null) {
            LOG.error("NextStatus:nextStatus" + nextStatus+ ", tableStatus" + JSON.toJSONString(tableStatus));
            // 保存但并不增加计数
            tableStatus.saveNotAddCnt();
            mttGameRecord.setProgresses(progresses);
            RecordUtils.restLastedRecord(mttGameRecord, tableStatus.getRoomId(), tableId);
            // 触发下一轮的发牌事件
            GameUtils.nextStatusTask(nextStatus, tableId, inningId,  self, context, sender);
            return;
        }
        String pwd = tableStatus.randomPwd();
        // 刷新set的property值
        tableStatus.save();
        // 发送底牌信息给玩家
        sendToUsers(map, tableId, tableStatus.getRoomId(), tableAllUser);
        // 发送给观看的玩家
        sendToOnlines(tableAllUser.getOnlineTableUserIds(), tableAllUser,
                tableStatus.getInningId(), tableStatus.getIndexCount(), tableId, tableStatus.getRoomId());

        //获取第一个下注的人 发送给玩家通知下注
        String betUserId = tableAllUser.getGamingAndExitUserMap().get(currentBetSitNum);

        TableUser betTableUser = TableUser.load(betUserId, tableId);
        //增加玩家的操作计数(默认为1)
        RedisUtil.setMap(GameConstants.USER_SPINGO_TABLE_KEY + betUserId + ":" + tableId, "operateCount", "1");
        if (!betTableUser.isExit()) { // 玩家没有退出挂机
            JSONObject object = new JSONObject();
            object.put("inner_id", tableStatus.getInningId());
            object.put("inner_cnt", tableStatus.getIndexCount());
            object.put("uid", betUserId);
            object.put("sn", Integer.parseInt(currentBetSitNum));
            object.put("t", Long.parseLong(betTableUser.getTotalSecs()));
            String[] ops = tableStatus.getUserCanOps(betUserId, currentBetSitNum);
            object.put("c_b", StringUtils.join(ops, ","));
            object.put("pwd", pwd);
            GameUtils.notifyUser(object, GameEventType.BET, betUserId, tableStatus.getRoomId());
        }

        progresses.add(MttGameRecord.Progress.create("bet", betTableUser.getSitNum(), "nil",
                System.currentTimeMillis() - mttGameRecord.getStartTime()));
        mttGameRecord.setProgresses(progresses);
        LOG.info("mttGameRecord" + JSON.toJSONString(mttGameRecord));
        RecordUtils.restLastedRecord(mttGameRecord, tableStatus.getRoomId(), tableId);

        // 通知其他玩家下注玩家需要下注
        JSONObject otherNextObject = new JSONObject();
        otherNextObject.put("inner_id", tableStatus.getInningId());
        otherNextObject.put("inner_cnt", tableStatus.getIndexCount());
        otherNextObject.put("uid", betUserId);
        otherNextObject.put("sn", Integer.parseInt(currentBetSitNum));
        otherNextObject.put("t", Long.parseLong(betTableUser.getTotalSecs()));
        otherNextObject.put("c_b", "");
        otherNextObject.put("pwd", "");
        try {
            Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
            userIds.remove(betUserId);
            // 如果不捕获异常则会终端下一个任务
            String newId = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(otherNextObject, GameEventType.BET, userIds, newId, tableStatus.getRoomId());
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // 触发30s的检测任务 大盲注下一位的状态 如果已经操作则跳过, preFlop
        GameUtils.triggerBetTestTask(context, tableId, inningId, currentBetSitNum, betUserId, 1, pwd, betTableUser.isExit(),
                Long.parseLong(betTableUser.getTotalSecs()), tableStatus.getRoomId());
    }

    private void sendToUsers(Map<String, JSONObject> map, String tableId, String gameId,TableAllUser tableAllUser) {
        for (Map.Entry<String, JSONObject> entry : map.entrySet()) {
             String userId = entry.getKey();
             if (!tableAllUser.isOnline(userId)) {
                 continue;
             }
             JSONObject object = entry.getValue();
             GameUtils.notifyUser(object, GameEventType.PREFLOP, userId, gameId);
        }
    }

    private void sendToOnlines(Collection<String> onlineUserIds, TableAllUser tableAllUser,
                               String innerId, String innerCnt, String tableId, String gameId) {
        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", innerCnt);
        object.put("hand", "");
        for (String userId : onlineUserIds) {
            if (!tableAllUser.userGamingExitHangSitUserId().containsValue(userId)) {
                GameUtils.notifyUser(object, GameEventType.PREFLOP, userId, gameId);
            }
        }
    }
}
