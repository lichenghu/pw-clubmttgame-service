package com.tiantian.clubmttgame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.tiantian.clubmtt.akka.event.game.GameStartEvent;
import com.tiantian.clubmttgame.akka.event.LocalTableRoundEndEvent;
import com.tiantian.clubmttgame.akka.event.TableTaskEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameConstants;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.model.*;
import com.tiantian.clubmttgame.utils.GameUtils;
import com.tiantian.clubmttgame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

/**
 *
 */
public class TableCheckChipsHandler implements EventHandler<TableTaskEvent> {
    static Logger LOG = LoggerFactory.getLogger(TableCheckChipsHandler.class);
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        String innerId = tableStatus.getInningId();
        String indexCount = tableStatus.getIndexCount();
        TableAllUser tableAllUser = TableAllUser.load(tableId);

        Collection<String> userIds = tableAllUser.userSitDownUsers().values();

        List<String> overList = Lists.newArrayList();


        MttGameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getRoomId(), tableStatus.getTableId());
        if (mttGameRecord == null) {
            mttGameRecord = new MttGameRecord();
        }
        List<MttGameRecord.Progress> progresses = mttGameRecord.getProgresses();

        Map<String, Long> userLeftChips = new TreeMap<>();
        for(String userId : userIds) {
            UserChips userChips = UserChips.load(userId, tableStatus.getRoomId());
            LOG.info("Users Chips:usrId:" + userId + ",chips" + JSON.toJSONString(userChips));
            if (userChips == null || userChips.getChips() == 0) {
                overList.add(userId);
                tableStatus.addGameOverUserId(userId);
            }
            userLeftChips.put(userId,  userChips == null ? 0 : userChips.getChips());
            tableStatus.addLeftUserChips(userId, userChips == null ? 0 : userChips.getChips());
        }
        LOG.info("All Users:" + JSON.toJSONString(userIds));
        LOG.info("Over List:" + JSON.toJSONString(overList));
        List<String> rebuyUserIds = new ArrayList<>();
        for (String userId : userIds) {
             TableUser tableUser = TableUser.load(userId, tableId);
             if (tableUser == null || tableUser.isNull()) {
                 continue;
             }
             String userTableId = tableUser.getTableId();
             if (tableId.equalsIgnoreCase(userTableId)) {
                 tableUser.setOperateCount(null);
                 tableUser.setBetStatus("");
                 tableUser.setShowCards("");
             }
             //判断筹码
             checkUserChips(userId, tableId,
                    tableUser.getSitNum(), tableAllUser.getOnlineTableUserIds(), tableUser,
                    innerId, indexCount, tableAllUser, tableStatus.getRoomId(), tableStatus, rebuyUserIds,
                     progresses, mttGameRecord.getStartTime());
        }

        if (rebuyUserIds.size() > 0) {
            //设置开始买入等待起始时间
            tableStatus.setRebuyStartMills(System.currentTimeMillis() + "");
            tableStatus.saveNotAddCnt();
            //  通知其他人有人买入
            sendWaitUserRebuy(rebuyUserIds, tableStatus, tableAllUser.getOnlineTableUserIds());

            GameUtils.userRebuyTask(tableId, tableStatus.getInningId(), context, tableStatus.getRoomId(),
                    overList, userLeftChips);
        }
        else {
            // 根据玩家allin 的先后顺序
            Map<String, Long>  userLeftRightChips = GameUtils.rankingUserChips(userLeftChips, tableStatus.userStartMoney(), tableStatus.userAllinSeq());
            tableStatus.clearNotFlush();
            tableStatus.saveNotAddCnt();
            context.parent().tell(new LocalTableRoundEndEvent(tableId, event.gameId(), overList, userLeftRightChips),
                    ActorRef.noSender());
        }


        progresses.add(MttGameRecord.Progress.create("end", "nil", "nil",
                System.currentTimeMillis() - mttGameRecord.getStartTime()));
        RecordUtils.restLastedRecord(mttGameRecord, tableStatus.getRoomId(), tableId);

    }

    private void checkUserChips(String userId, String tableId,
                                String sitNum, Collection<String> toUserIds, TableUser tableUser,
                                String innerId, String innerCnt, TableAllUser tableAllUser,
                                String gameId, TableStatus tableStatus, List<String> rebuyUserIds,
                                List<MttGameRecord.Progress> progresses, long startTime) {
        UserChips userChips = UserChips.load(userId, gameId);
        if (userChips != null && userChips.getChips() == 0) {
            if (checkRebuy(userId, tableStatus, userChips)) { //通知玩家买入
                LOG.info("checkRebuy ok");
                rebuyUserIds.add(userId);
                tableStatus.addRebuyUserIds(userId); // 添加买入玩家
                tableUser.save();
            }
            else { // 不能买入
                userChips.delSelf(gameId); //删除筹码
                if (tableUser == null || tableUser.isNull()
                        || "standing".equalsIgnoreCase(tableUser.getStatus())) {
                    return;
                }
                tableAllUser.deleteUserByUserId(userId);
                // 站起
                tableUser.delSelf(tableUser.getTableId());
               // tableAllUser.userStandUp(userId);
                // 通知玩家离开
                JSONObject object1 = new JSONObject();
                object1.put("inner_id", innerId);
                object1.put("inner_cnt", innerCnt);
                object1.put("uid", userId);
                object1.put("sn", Integer.parseInt(sitNum));
                object1.put("reason", "game_over");
                String id = UUID.randomUUID().toString().replace("-", "");
                // 如果不捕获异常则会终端下一个任务
                GameUtils.notifyUsers(object1, GameEventType.STAND_UP, toUserIds, id, gameId);
                progresses.add(MttGameRecord.Progress.create("stand_up", sitNum, "nil",
                        System.currentTimeMillis() - startTime));
            }
        }
    }


    private boolean checkRebuy(String userId, TableStatus tableStatus,  UserChips userChips) {
        if (userChips == null) {
            LOG.info("checkRebuy userChips is null");
            return false;
        }
        String blindLvl = tableStatus.getMaxCanRebuyBlindLvl();
        if (StringUtils.isBlank(blindLvl)) {
            LOG.info("checkRebuy MaxCanRebuyBlindLvl is null");
            return false;
        }
        String buyInCnt = tableStatus.getReBuyInCnt();
        if (StringUtils.isBlank(buyInCnt)) {
            LOG.info("checkRebuy buyInCnt is null");
            return false;
        }
        if (Integer.valueOf(tableStatus.getCurrentBlindLevel()) > Integer.valueOf(blindLvl)) {
            LOG.info("checkRebuy CurrentBlindLevel is gt blindLvl");
            return false;
        }
        if (userChips.getBuyInCnt() >= Integer.valueOf(tableStatus.getReBuyInCnt())) {
            LOG.info("checkRebuy BuyInCnt  is gt ReBuyInCnt");
            return false;
        }
        GameStartEvent.Game game = tableStatus.game();
        int currentLvl = Integer.valueOf(tableStatus.getCurrentBlindLevel());
        if (currentLvl > game.getRules().size()) {
            LOG.info("checkRebuy currentLvl is gt game.getRules().size");
            return false;
        }

        boolean isDelaySign = tableStatus.isDelayingSign();
        // 判断当前人数是否达到了钱圈,钱圈不能Rebuy
        String leftUsers = tableStatus.getLeftUsers();
        LOG.info("leftUsers:" + leftUsers);
        if (StringUtils.isNotBlank(leftUsers)) {
            if (Integer.parseInt(leftUsers) <= 9) {
                if (!isDelaySign) { //不在在延迟报名中
                    return false;
                }
            }
        }
        GameStartEvent.Rule rule = game.getRules().get(Integer.valueOf(tableStatus.getCurrentBlindLevel()) - 1);
        rule.setCostMoney(game.getPoolFee() + game.getTaxFee());
        JSONObject object1 = new JSONObject();
        object1.put("inner_id", tableStatus.getInningId());
        object1.put("inner_cnt", tableStatus.getIndexCount());
        object1.put("user_id", userId);
        object1.put("cost", rule.getCostMoney());
        object1.put("chips", rule.getReBuyIn());
        object1.put("left_times", Integer.valueOf(tableStatus.getReBuyInCnt()) - userChips.getBuyInCnt());
        object1.put("secs", GameConstants.USER_REBUY_TIME/1000);
        GameUtils.notifyUser(object1, GameEventType.MTT_USER_REBUY, userId, tableStatus.getRoomId());
        return true;
    }

    private void sendWaitUserRebuy(List<String> rebuyUserIds, TableStatus tableStatus, Collection<String> userIds) {
        JSONObject object1 = new JSONObject();
        object1.put("inner_id", tableStatus.getInningId());
        object1.put("inner_cnt", tableStatus.getIndexCount());
        for (String userId : userIds) {
            if (!rebuyUserIds.contains(userId)) {
                GameUtils.notifyUser(object1, GameEventType.WAIT_REBUY, userId, tableStatus.getRoomId());
            }
        }
    }
}
