package com.tiantian.clubmttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmtt.akka.event.user.GameUserCancelRebuyEvent;
import com.tiantian.clubmttgame.akka.event.LocalTableRoundEndEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import com.tiantian.clubmttgame.manager.model.TableUser;
import com.tiantian.clubmttgame.manager.model.UserChips;
import com.tiantian.clubmttgame.utils.GameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class UserCancleRebuyHandler implements EventHandler<GameUserCancelRebuyEvent> {
    static Logger LOG = LoggerFactory.getLogger(UserCancleRebuyHandler.class);
    @Override
    public void handler(GameUserCancelRebuyEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.userId();
        String tableId = event.getTableId();
        String gameId = event.gameId();
        // 判断买入次数和能否买入
        TableStatus tableStatus = TableStatus.load(tableId);
        if(tableStatus == null) {
            LOG.info("UserRebuy: tableStatus is null");
            sender.tell(true, ActorRef.noSender());
            return;
        }
        if (!tableStatus.hasUserRebuy(userId)) {
            sender.tell(true, ActorRef.noSender());
            return;
        }
        UserChips userChips = UserChips.load(userId, gameId);
        if (userChips != null && userChips.getChips() == 0) {
            userChips.delSelf(gameId); //删除筹码
        }
        TableUser tableUser = TableUser.load(userId, event.getTableId());
        if (tableUser == null || tableUser.isNull()
                || "standing".equalsIgnoreCase(tableUser.getStatus())) {
            sender.tell(true, ActorRef.noSender());
            return;
        }
        String sitNum = tableUser.getSitNum();
        // 站起
        tableUser.delSelf(tableUser.getTableId());
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        tableAllUser.deleteUserByUserId(userId);

        String innerId = tableStatus.getInningId();
        String innerCnt = tableStatus.getIndexCount();
        // 通知玩家离开
        JSONObject object1 = new JSONObject();
        object1.put("inner_id", innerId);
        object1.put("inner_cnt", innerCnt);
        object1.put("uid", userId);
        object1.put("sn", Integer.parseInt(sitNum));
        object1.put("reason", "");
        String id = UUID.randomUUID().toString().replace("-", "");
        // 如果不捕获异常则会终端下一个任务
        GameUtils.notifyUsers(object1, GameEventType.STAND_UP, tableAllUser.getOnlineTableUserIds(), id, gameId);

        // 取消掉rebuy
        int leftRebuy = tableStatus.reduceRebuyUserId(userId);
        if (leftRebuy == 0) { // 开始游戏
            List<String> overList = tableStatus.overUserList();
            Map<String, Long> userLeftChips = tableStatus.userLeftChipsMap();

            Map<String, Long> userLeftRightChips = GameUtils.rankingUserChips(userLeftChips, tableStatus.userStartMoney(), tableStatus.userAllinSeq());

            tableStatus.clearNotFlush();
            tableStatus.save();

            context.parent().tell(new LocalTableRoundEndEvent(tableId, event.gameId(), overList, userLeftRightChips),
                    ActorRef.noSender());
        }

        sender.tell(true, ActorRef.noSender());
    }
}
