package com.tiantian.clubmttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmtt.akka.event.user.GameUserCheckEvent;
import com.tiantian.clubmtt.akka.event.user.GameUserFoldEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.handlers.Handlers;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.constants.GameStatus;
import com.tiantian.clubmttgame.manager.model.*;
import com.tiantian.clubmttgame.utils.GameUtils;
import com.tiantian.clubmttgame.utils.RecordUtils;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 *
 */
public class UserCheckHandler implements EventHandler<GameUserCheckEvent> {
    @Override
    public void handler(GameUserCheckEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String pwd = event.getPwd();
        String tableId = event.getTableId();
        TableUser tableUser = TableUser.load(userId, event.getTableId());
        if (tableUser.getSitNum() != null ) {
            TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
            // 判断状态
            if (tableStatus != null && tableStatus.checkPwd(pwd)) {
                // 校验玩家是否能够让牌
                UserChips userChips = UserChips.load(userId, tableStatus.getRoomId());
                String[] ops = tableStatus.getUserCanOps(userId, tableUser.getSitNum());
                String status = null;
                char op = ops[0].charAt(1);
                // 能不能看牌 则弃牌
                if ('0' == op) {
                    status = GameEventType.FOLD;
                    GameUserFoldEvent gameUserFoldEvent = new GameUserFoldEvent(userId, pwd, tableStatus.getRoomId());
                    gameUserFoldEvent.setTbId(tableId);
                    Handlers.INSTANCE.execute(gameUserFoldEvent, self, context, sender);
                    return;
                } else {
                    status = GameEventType.CHECK;
                }
                // 增加玩家的操作计数
                tableUser.addOneOperateCount();
                tableUser.clearNotOperateCount();
                tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), status);
                //设置玩家让牌
                tableUser.setBetStatus(status);
                tableUser.save();
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("uid", userId);
                object.put("sn", Integer.parseInt(tableUser.getSitNum()));
                object.put("left_chips", userChips.getChips());
                //发送个桌子的玩家
                TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
                Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
                String id = UUID.randomUUID().toString().replace("-", "");
                GameUtils.notifyUsers(object, status, userIds, id, tableStatus.getRoomId());

                //通知玩家操作成功
                JSONObject object1 = new JSONObject();
                object1.put("inner_id", tableStatus.getInningId());
                object1.put("inner_cnt", tableStatus.getIndexCount());
                object1.put("op", status);
                GameUtils.notifyUser(object1, "call_suc", userId, tableStatus.getRoomId());

                MttGameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getRoomId(), tableStatus.getTableId());
                if (mttGameRecord == null) {
                    mttGameRecord = new MttGameRecord();
                }
                List<MttGameRecord.Progress> progresses = mttGameRecord.getProgresses();
                progresses.add(MttGameRecord.Progress.create("check", tableUser.getSitNum(), null,
                        System.currentTimeMillis() - mttGameRecord.getStartTime()));
                RecordUtils.restLastedRecord(mttGameRecord, tableStatus.getRoomId(), tableId);

                // 判断游戏轮次
                GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, tableStatus.getStatus(), tableStatus.getMaxBetSitNum(),
                        tableUser.getSitNum());
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    tableStatus.save();
                    // 通知玩家的池信息
                    GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                    // 触发下一轮的发牌事件
                    GameUtils.triggerNextStatusTask(nextStatus, tableUser.getTableId(), tableStatus.getInningId(),
                            context, tableStatus.getRoomId());
                    return;
                }
                //通知下一个玩家下注
                GameUtils.noticeNextUserBet(tableUser.getTableId(), tableStatus, null, false, null,
                        self, context, sender);
            }
        }
    }
}
