package com.tiantian.clubmttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmtt.akka.event.user.GameUserAllinEvent;
import com.tiantian.clubmtt.akka.event.user.GameUserCheckEvent;
import com.tiantian.clubmtt.akka.event.user.GameUserFoldEvent;
import com.tiantian.clubmtt.akka.event.user.GameUserRaiseEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.handlers.Handlers;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.constants.GameStatus;
import com.tiantian.clubmttgame.manager.model.*;
import com.tiantian.clubmttgame.utils.GameUtils;
import com.tiantian.clubmttgame.utils.RecordUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class UserRaiseHandler implements EventHandler<GameUserRaiseEvent> {
    @Override
    public void handler(GameUserRaiseEvent userRaiseEvent, ActorRef self, UntypedActorContext context, ActorRef sender) {

        String userId = userRaiseEvent.getUserId();
        long raise = userRaiseEvent.getRaise();
        String tableId = userRaiseEvent.getTableId();
        String pwd = userRaiseEvent.getPwd();
        TableUser tableUser = TableUser.load(userId, userRaiseEvent.getTableId());
        if (tableUser.getSitNum() != null ) {
            TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
            if(raise < 0) {
                System.out.println("raise is zero : "+ JSON.toJSONString(tableStatus));
            }
            // 判断状态
            if (tableStatus != null && tableStatus.checkPwd(pwd)) {
                //校验玩家能否加注，然后加注
                UserChips userChips = UserChips.load(userId, tableStatus.getRoomId());

                String[] ops = tableStatus.getUserCanOps(userId, tableUser.getSitNum());
                String status = null;
                char op = ops[0].charAt(3);
                long minRaiseChips = Long.parseLong(ops[1]);
                // 能不能加注
                if ('0' == op || minRaiseChips > raise) {
                    // 判断是否能够看牌
                    char checkOp = ops[0].charAt(1);
                    if ('0' == checkOp) {
                        status = GameEventType.FOLD;
                        GameUserFoldEvent gameUserFoldEvent = new GameUserFoldEvent(userId, pwd, tableStatus.getRoomId());
                        gameUserFoldEvent.setTbId(tableId);
                        Handlers.INSTANCE.execute(gameUserFoldEvent, self, context, sender);
                        return;
                    } else {
                        status = GameEventType.CHECK;
                        GameUserCheckEvent gameUserCheckEvent = new GameUserCheckEvent(userId, pwd, tableStatus.getRoomId());
                        gameUserCheckEvent.setTbId(tableId);
                        Handlers.INSTANCE.execute(gameUserCheckEvent, self, context, sender);
                        return;
                    }
                } else {
                    if (raise >= userChips.getChips()) {
                        raise = userChips.getChips();
                        status = GameEventType.ALLIN;
                        GameUserAllinEvent gameUserAllinEvent = new GameUserAllinEvent(userId, pwd, tableStatus.getRoomId());
                        gameUserAllinEvent.setTbId(tableId);
                        Handlers.INSTANCE.execute(gameUserAllinEvent, self, context, sender);
                        return;
                    } else {
                        status = GameEventType.RAISE;
                    }
                }

                tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), status);
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("uid", userId);
                object.put("sn", Integer.parseInt(tableUser.getSitNum()));
                if (status.equalsIgnoreCase(GameEventType.RAISE) ||
                        status.equalsIgnoreCase(GameEventType.ALLIN)) {
                    userChips.reduceAndFlushChips(raise);
                    object.put("val", raise);
                    tableStatus.userBet(tableUser.getSitNum(), raise);
                    // 更新平均加注日志
                    tableStatus.updateUserBetRaiseLog(tableUser.getSitNum(), raise);
                }
                object.put("left_chips", userChips.getChips());
                object.put("bottom_pool", tableStatus.getTotalPoolMoney());
                tableUser.addOneOperateCount();
                tableUser.clearNotOperateCount();
                //设置玩家加注
                tableUser.setBetStatus(status);
                tableUser.save();
                if (status.equalsIgnoreCase(GameEventType.ALLIN)) {
                    tableStatus.addUserAllinSeq(userId);
                }
                // 通知其他在玩家
                TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
                Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
                String id = UUID.randomUUID().toString().replace("-", "");
                GameUtils.notifyUsers(object, status, userIds, id, tableStatus.getRoomId());

                // 通知玩家操作成功
                JSONObject object1 = new JSONObject();
                object1.put("inner_id", tableStatus.getInningId());
                object1.put("inner_cnt", tableStatus.getIndexCount());
                object1.put("op", status);
                GameUtils.notifyUser(object1, "call_suc", userId, tableStatus.getRoomId());


                MttGameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getRoomId(), tableStatus.getTableId());
                if (mttGameRecord == null) {
                    mttGameRecord = new MttGameRecord();
                }
                List<MttGameRecord.Progress> progresses = mttGameRecord.getProgresses();
                progresses.add(MttGameRecord.Progress.create("raise", tableUser.getSitNum(),  raise + "",
                        System.currentTimeMillis() - mttGameRecord.getStartTime()));
                RecordUtils.restLastedRecord(mttGameRecord, tableStatus.getRoomId(), tableId);

                // 判断游戏轮次
                GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, tableStatus.getStatus(), tableStatus.getMaxBetSitNum(),
                        tableUser.getSitNum());
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    if(status.equalsIgnoreCase(GameEventType.ALLIN)
                            || status.equalsIgnoreCase(GameEventType.FOLD)) {
                        boolean needShow = tableStatus.needShowAllCards();
                        if (needShow) { // 需要显示所有的手牌
                            List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                            JSONObject object2 = new JSONObject();
                            object2.put("inner_id", tableStatus.getInningId());
                            object2.put("inner_cnt", tableStatus.getIndexCount());
                            object2.put("all_cards", cardsList);
                            String id2 = UUID.randomUUID().toString().replace("-", "");
                            GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableStatus.getRoomId());
                        }
                    }
                    // 通知玩家的池信息
                    GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                    tableStatus.save();
                    // 触发下一轮的发牌事件
                    GameUtils.triggerNextStatusTask(nextStatus, tableUser.getTableId(), tableStatus.getInningId(), context,
                            tableStatus.getRoomId());
                    return;
                }
                // 玩家下的最大注
                String maxBetSitNumChips = tableStatus.getMaxBetSitNumChips();
                String maxBetSitNum = tableStatus.getMaxBetSitNum();

                //通知下一个玩家下注
                GameUtils.noticeNextUserBet(tableUser.getTableId(), tableStatus, maxBetSitNum, false, maxBetSitNumChips,
                        self, context, sender);
            }
        }
    }
}
