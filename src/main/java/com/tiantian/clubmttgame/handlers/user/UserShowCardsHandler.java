package com.tiantian.clubmttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmtt.akka.event.user.GameUserShowCardsEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.constants.GameStatus;
import com.tiantian.clubmttgame.manager.model.MttGameRecord;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import com.tiantian.clubmttgame.manager.model.TableUser;
import com.tiantian.clubmttgame.utils.GameUtils;
import com.tiantian.clubmttgame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.UUID;

/**
 *
 */
public class UserShowCardsHandler implements EventHandler<GameUserShowCardsEvent> {
    @Override
    public void handler(GameUserShowCardsEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.getTableId();
        String hands = event.getData();
        TableUser tableUser = TableUser.load(userId, event.getTableId());
        if (tableUser == null) {
            return;
        }
        TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
        // 是在结算的时候直接亮牌
        if (tableStatus != null && GameStatus.FINISH.name().equalsIgnoreCase(tableStatus.getStatus())) {
            String handCardsStr = tableStatus.getUserHandCards(tableUser.getSitNum());
            if (StringUtils.isBlank(handCardsStr)) {
                return;
            }
            String[] handCardStrs = handCardsStr.split(",");
            String handCards = handCardsStr;
            if (StringUtils.isNotBlank(hands)) {
                if ("1".equalsIgnoreCase(hands)) {
                    handCards =  handCardStrs[0] + ",";
                } else if ("2".equalsIgnoreCase(hands)) {
                    handCards = "," + handCardStrs[1];
                }
            }
            TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
            JSONObject object = new JSONObject();
            object.put("inner_id", tableStatus.getInningId());
            object.put("inner_cnt", tableStatus.getIndexCount());
            object.put("uid", userId);
            object.put("sn", Integer.parseInt(tableUser.getSitNum()));
            object.put("hand_cards", handCards);
            String id = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(object, GameEventType.SHOW_CARDS, tableAllUser.getOnlineTableUserIds(), id, tableStatus.getRoomId());

            MttGameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getRoomId(), tableStatus.getTableId());
            if (mttGameRecord == null) {
                mttGameRecord = new MttGameRecord();
            }
            List<MttGameRecord.Progress> progresses = mttGameRecord.getProgresses();
            progresses.add(MttGameRecord.Progress.create("show_card", tableUser.getSitNum(),  handCards ,
                    System.currentTimeMillis() - mttGameRecord.getStartTime()));
            mttGameRecord.addSitShowCardSit(tableUser.getSitNum(), handCards);
            RecordUtils.restLastedRecord(mttGameRecord, tableStatus.getRoomId(), tableId);
        }
    }
}
