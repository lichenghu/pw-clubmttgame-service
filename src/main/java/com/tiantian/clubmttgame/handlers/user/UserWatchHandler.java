package com.tiantian.clubmttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.clubmtt.akka.event.user.GameUserWatchEvent;
import com.tiantian.clubmttgame.akka.event.LocalGameOnlineWatchEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.handlers.helper.UserInfHandlerHelper;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class UserWatchHandler implements EventHandler<GameUserWatchEvent> {
    private static Logger LOG = LoggerFactory.getLogger(UserWatchHandler.class);
    @Override
    public void handler(GameUserWatchEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        TableAllUser tableAllUser = TableAllUser.load(event.getTableId());
        if (tableAllUser != null) {
            tableAllUser.joinOnline(event.getUserId());
            TableStatus tableStatus = TableStatus.load(event.getTableId());
            if (tableStatus == null) {
                sender.tell(false, ActorRef.noSender());
                return;
            }
            UserInfHandlerHelper.sendUserInfos(tableStatus, tableAllUser, event.getTableId(), event.userId(), null, null, null);
            sender.tell(true, ActorRef.noSender());
            context.parent().tell(new LocalGameOnlineWatchEvent(event.userId(), event.getTableId()), ActorRef.noSender());
        } else {
            sender.tell(false, ActorRef.noSender());
        }
    }
}
