package com.tiantian.clubmttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmtt.akka.event.user.GameUserReturnSitEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameConstants;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import com.tiantian.clubmttgame.manager.model.TableUser;
import com.tiantian.clubmttgame.manager.model.UserChips;
import com.tiantian.clubmttgame.utils.GameUtils;

import java.util.UUID;

/**
 *
 */
public class UserReturnSitHandler implements EventHandler<GameUserReturnSitEvent> {
    @Override
    public void handler(GameUserReturnSitEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        TableUser tableUser = TableUser.load(event.userId(), event.getTableId());
        if (!tableUser.isNull()) {
            if (!tableUser.isHang()) {
                sender.tell(true, ActorRef.noSender());
                return;
            }
            tableUser.setStatus(GameConstants.USER_STATUS_WAIT);
            tableUser.setNotOperateCount("");
            tableUser.save();
            UserChips userChips = UserChips.load(event.userId(), event.gameId());
            if (userChips != null) {
                userChips.resetStatus(GameConstants.USER_STATUS_WAIT);
            }
            TableAllUser tableAllUser = TableAllUser.load(event.getTableId());
            TableStatus tableStatus = TableStatus.load(event.getTableId());
            if (tableStatus != null) {
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("user_id", event.userId());
                object.put("sn", Integer.parseInt(tableUser.getSitNum()));
                String id = UUID.randomUUID().toString().replace("-", "");
                GameUtils.notifyUsers(object, GameEventType.MTT_USER_RETURN, tableAllUser.getOnlineTableUserIds(), id, tableStatus.getRoomId());
            }
        } else {
            sender.tell(false, ActorRef.noSender());
            return;
        }
        sender.tell(true, ActorRef.noSender());

    }
}
