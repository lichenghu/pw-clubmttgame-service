package com.tiantian.clubmttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.club.proxy_client.ClubIface;
import com.tiantian.club.thrift.UserClub;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.clubmtt.akka.event.game.GameStartEvent;
import com.tiantian.clubmtt.akka.event.user.GameUserRebuyEvent;
import com.tiantian.clubmttgame.akka.event.LocalTableRoundEndEvent;
import com.tiantian.clubmttgame.akka.event.LocalUserRebuyEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import com.tiantian.clubmttgame.manager.model.TableUser;
import com.tiantian.clubmttgame.manager.model.UserChips;
import com.tiantian.clubmttgame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class UserRebuyHandler implements EventHandler<GameUserRebuyEvent> {
    static Logger LOG = LoggerFactory.getLogger(UserRebuyHandler.class);
    private static final int MONEY_EXCHANGE = 100;
    @Override
    public void handler(GameUserRebuyEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.userId();
        String tableId = event.getTableId();
        String gameId = event.gameId();
        String clubId = event.getClubId();
        UserChips userChips = UserChips.load(userId, gameId);
        if (userChips == null) {
            LOG.info("UserRebuy: userChips is null");
            sender.tell(-1, ActorRef.noSender());
            return;
        }
        // 判断买入次数和能否买入
        TableStatus tableStatus = TableStatus.load(tableId);
        if(tableStatus == null) {
            LOG.info("UserRebuy: tableStatus is null");
            sender.tell(-2, ActorRef.noSender());
            return;
        }
        String blindLvl = tableStatus.getMaxCanRebuyBlindLvl();
        if(StringUtils.isBlank(blindLvl)) {
           LOG.info("UserRebuy: blindLvl is null");
           sender.tell(-3, ActorRef.noSender());
           return;
        }
        String buyInCnt = tableStatus.getReBuyInCnt();
        if (StringUtils.isBlank(buyInCnt)) {
            LOG.info("UserRebuy: buyInCnt is null");
            sender.tell(-4, ActorRef.noSender());
            return;
        }
        if (!tableStatus.canRebuy()) {
            LOG.info("UserRebuy: CurrentBlindLevel gt blindLvl" + tableStatus.getCurrentBlindLevel() + ":" + blindLvl);
            sender.tell(-5, ActorRef.noSender());
            return;
        }
        if ( userChips.getBuyInCnt() >= Integer.valueOf(tableStatus.getReBuyInCnt())) {
            LOG.info("UserRebuy: BuyInCnt is gt tableStatus.getReBuyInCnt ," +userChips.getBuyInCnt() + ":" + tableStatus.getReBuyInCnt());
            sender.tell(-6, ActorRef.noSender());
            return;
        }
        GameStartEvent.Game game = tableStatus.game();
        if (game == null) {
            LOG.info("UserRebuy: game is null");
            sender.tell(-7, ActorRef.noSender());
            return;
        }
        if (!tableStatus.hasUserRebuy(userId)) {
            LOG.info("UserRebuy: has no UserRebuy," + tableStatus.getRebuyUserIds());
            sender.tell(-8, ActorRef.noSender());
            return;
        }
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        TableUser tableUser = TableUser.load(userId, tableId);
        if (tableUser == null || tableUser.isNull()) {
            LOG.info("UserRebuy: htableUser is null");
            sender.tell(-9, ActorRef.noSender());
            return;
        }
        boolean isDelaySign = tableStatus.isDelayingSign();
        String leftUsers = tableStatus.getLeftUsers();
        if (StringUtils.isNotBlank(leftUsers)) {
            if (Integer.parseInt(leftUsers) <= 9) {
                if (!isDelaySign) { //不在在延迟报名中
                    LOG.info("UserRebuy: leftUsers gt rewards");
                    sender.tell(-9, ActorRef.noSender());
                    return;
                }
            }
        }

        int leftRebuy = tableStatus.reduceRebuyUserId(userId);
        GameStartEvent.Rule rule =  game.getRules().get(Integer.valueOf(tableStatus.getCurrentBlindLevel()) - 1);
        long rebuyChips = (long)rule.getReBuyIn();
        if (rebuyChips <= 0) {
            sender.tell(-9, ActorRef.noSender());
            return;
        }
        try {
            rule.setCostMoney(game.getPoolFee() + game.getTaxFee()); // 设置报名费

            UserClub userClub = ClubIface.instance().iface().getUserClub(userId, clubId);
            if (userClub.getScore() < rule.getCostMoney()) {
                sender.tell(-10, ActorRef.noSender()); // 金币不足
                checkGameStart(context, leftRebuy, tableStatus, tableUser, tableId, tableAllUser, userId);
                return;
            }

            boolean ret = AccountIface.instance().iface().reduceUserMoney(userId, rule.getCostMoney() * MONEY_EXCHANGE);
            if (!ret) {
                sender.tell(-10, ActorRef.noSender()); // 金币不足
                checkGameStart(context, leftRebuy, tableStatus, tableUser, tableId, tableAllUser, userId);
                return;
            }

            boolean feeRet = ClubIface.instance().iface().reduceUserClubScore(userId, clubId, rule.getCostMoney());
            if (!feeRet) {
                sender.tell(-10, ActorRef.noSender()); // 金币不足
                checkGameStart(context, leftRebuy, tableStatus, tableUser, tableId, tableAllUser, userId);
                return;
            }
        } catch (TException e) {
            e.printStackTrace();
            sender.tell(-11, ActorRef.noSender());
            return;
        }
        userChips.addBuyInAndCnt(rebuyChips, 1);
        // 发送买入消息到room manager
        context.parent().tell(new LocalUserRebuyEvent(game.getGameId(), tableId, userId, rebuyChips), ActorRef.noSender());

        tableStatus.removeGameOverUserId(userId); // 从over集合中移除该玩家
        tableStatus.addLeftUserChips(userId, rebuyChips); // 设置该玩家的剩余筹码

        checkGameStart(context, leftRebuy, tableStatus, tableUser, tableId, tableAllUser, userId);

        sender.tell(0, ActorRef.noSender());
    }

    private void checkGameStart(UntypedActorContext context, int leftRebuy, TableStatus tableStatus,
                                TableUser tableUser, String tableId, TableAllUser tableAllUser, String userId) {
        String innerId = tableStatus.getInningId();
        String indexCount = tableStatus.getIndexCount();
        String gameId = tableStatus.getRoomId();
        // 判断玩家筹码是否为0
        checkUserChips(userId,
                tableUser.getSitNum(), tableAllUser.getOnlineTableUserIds(), tableUser,
                innerId, indexCount, tableAllUser, tableStatus.getRoomId());
        if (leftRebuy == 0) { // 开始游戏
            List<String> overList = tableStatus.roundGameOverUserId();
            Map<String, Long> userLeftChips = tableStatus.userLeftChipsMap();
            Map<String, Long>  userLeftRightChips = GameUtils.rankingUserChips(userLeftChips, tableStatus.userStartMoney(), tableStatus.userAllinSeq());

            tableStatus.clearNotFlush();  // 必须放在当前
            tableStatus.saveNotAddCnt();
            // 发送消息开始游戏
            context.parent().tell(new LocalTableRoundEndEvent(tableId, gameId, overList, userLeftRightChips),
                    ActorRef.noSender());
            return;
        }
        tableStatus.saveNotAddCnt();
    }

    private void checkUserChips(String userId,
                                String sitNum, Collection<String> toUserIds, TableUser tableUser,
                                String innerId, String innerCnt, TableAllUser tableAllUser,
                                String gameId) {
        UserChips userChips = UserChips.load(userId, gameId);
        if (userChips != null && userChips.getChips() == 0) {
            userChips.delSelf(gameId); //删除筹码
            if (tableUser == null || tableUser.isNull()
                    || "standing".equalsIgnoreCase(tableUser.getStatus())) {
                return;
            }
            // 站起
            tableUser.delSelf(tableUser.getTableId());
            // tableAllUser.userStandUp(userId);
            tableAllUser.deleteUserByUserId(userId);
            // 通知玩家离开
            JSONObject object1 = new JSONObject();
            object1.put("inner_id", innerId);
            object1.put("inner_cnt", innerCnt);
            object1.put("uid", userId);
            object1.put("sn", Integer.parseInt(sitNum));
            object1.put("reason", "");
            String id = UUID.randomUUID().toString().replace("-", "");
            // 如果不捕获异常则会终端下一个任务
            GameUtils.notifyUsers(object1, GameEventType.STAND_UP, toUserIds, id, gameId);
        } else {
            tableUser.setOperateCount(null);
            tableUser.setBetStatus("");
            tableUser.setShowCards("");
            tableUser.save();
        }
    }
}
