package com.tiantian.clubmttgame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmtt.akka.event.user.GameUserEmojiEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import com.tiantian.clubmttgame.utils.GameUtils;
import java.util.UUID;

/**
 *
 */
public class UserEmojiHandler implements EventHandler<GameUserEmojiEvent> {
    @Override
    public void handler(GameUserEmojiEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String fromUserId = event.userId();
        String toUserId = event.getToUserId();
        String emoji = event.getEmoji();
        String tableId = event.getTableId();
        String innerId = "";
        String innerCnt = "";
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        //通知玩家操作成功
        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", innerCnt);
        object.put("from_user_id", fromUserId);
        object.put("to_user_id", toUserId);
        object.put("emoji", emoji);
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        if (tableAllUser == null) {
            return;
        }
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.MTT_EMOJI, tableAllUser.getOnlineTableUserIds(), id
                , tableStatus.getRoomId());
    }
}
