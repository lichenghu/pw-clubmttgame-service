package com.tiantian.clubmttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmtt.akka.event.game.GameStartEvent;
import com.tiantian.clubmttgame.akka.event.LocalUserJoinEvent;
import com.tiantian.clubmttgame.akka.event.TableTaskEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.handlers.helper.UserInfHandlerHelper;
import com.tiantian.clubmttgame.manager.constants.GameConstants;
import com.tiantian.clubmttgame.manager.constants.GameStatus;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import com.tiantian.clubmttgame.manager.model.TableUser;
import com.tiantian.clubmttgame.manager.model.UserChips;
import com.tiantian.clubmttgame.utils.GameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

/**
 * 当前桌子等待其他玩家加入的时候,有玩家加入游戏
 */
public class TableUserJoinHandler implements EventHandler<LocalUserJoinEvent> {
    static Logger LOG = LoggerFactory.getLogger(TableUserJoinHandler.class);
    @Override
    public void handler(LocalUserJoinEvent event, ActorRef self,
                        UntypedActorContext context, ActorRef sender) {

        String joinUserId = event.getJoinUserId();
        String nickName = event.getNickName();
        String avatarUrl = event.getAvatarUrl();
        String gender = event.getGender();
        String tableId = event.tableId();
        String gameId = event.gameId();
        int leftUser = event.getLeftUser();
        long averageChips = event.getAverageChips();
        long nextUpBlindTimes = event.getNextUpBlindTimes();
        int currentBlindLevel = event.getCurrentBlindLevel();
        GameStartEvent.Game game = event.getGame();
        boolean canStart = event.isCanStart();
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) { //  创建桌子
            tableStatus = TableStatus.init(tableId);
            tableStatus.setRoomId(game.getGameId());
            tableStatus.setGameInfo(JSON.toJSONString(game));
            tableStatus.setInningId(UUID.randomUUID().toString().replace("-", ""));
            tableStatus.setStatus(GameStatus.INIT.name());
            tableStatus.setMinUsers(6 + "");
            tableStatus.setMaxUsers(game.getTableUsers() + "");
            tableStatus.gameStop(); // 游戏未开始
            tableStatus.setLeftUsers(leftUser + "");
            tableStatus.setAverageChips(averageChips + "");
            tableStatus.setMaxCanRebuyBlindLvl(game.getCanRebuyBlindLvl() + "");
            tableStatus.setReBuyInCnt(game.getBuyInCnt() + "");
            tableStatus.setCurrentBlindLevel(currentBlindLevel + ""); //设置盲注级别
            tableStatus.setNextUpBlindTimes(nextUpBlindTimes + "");
            tableStatus.saveNotAddCnt();
        }
        String maxUsers = tableStatus.getMaxUsers();
        // 处理玩家初始化和加入
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
        String sitNum = getNextSitNum(tableAllUser, Integer.parseInt(maxUsers)) + "";
        TableUser tableUser = TableUser.init(joinUserId, tableId);
        tableUser.setSitNum(sitNum);
        tableUser.setRoomId(event.getGameId());
        tableUser.setNickName(nickName);
        tableUser.setAvatarUrl(avatarUrl);
        tableUser.setGender(gender);
        tableUser.setStatus(GameConstants.USER_STATUS_GAMING);
        tableUser.setTotalSecs(GameConstants.BET_DELAYER_TIME / 1000 + "");
        tableUser.save();
        tableAllUser.joinUser(joinUserId, sitNum, true);
        tableAllUser = TableAllUser.load(tableId); // 重新刷新数据
        long beginChips = 0;
        // 初始化筹码,和 gameId 关联
        UserChips.init(joinUserId, gameId, beginChips);

        // 推送user_info给加入者
        UserInfHandlerHelper.sendUserInfos(tableStatus, tableAllUser, tableId, joinUserId, nickName, avatarUrl, gender);
        // 推送join_room给其他人
        UserInfHandlerHelper.sendJoinRoomInfo(tableStatus, gameId, beginChips, tableUser,
                tableAllUser.getOnlineTableUserIds());
        if (canStart) {
            tableStatus.setStatus(GameStatus.READY.name());
            tableStatus.setStartMills(System.currentTimeMillis() + "");
            tableStatus.saveNotAddCnt();

            //随机一个庄位
            JSONObject object2 = new JSONObject();
            object2.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
            object2.put("tableId", tableId);
            object2.put("inningId", tableStatus.getInningId());
            object2.put("gameId", tableStatus.getRoomId());
            Random random = new Random();
            List<String> userIdList = new ArrayList<>(userIds);
            int randomIndex = random.nextInt(userIdList.size());
            object2.put("randomBtn",  userIdList.get(randomIndex));
            GameUtils.pushTask(context, new TableTaskEvent(GameStatus.BEGIN.name(), object2), 1000);
          //  Handlers.INSTANCE.execute(new TableTaskEvent(GameStatus.BEGIN.name(), object2), self, context, sender);
        } else {
            //TODO 推送等待其他玩家加入消息
        }
    }
    private int getNextSitNum(TableAllUser tableAllUser, int maxUsers) {
         Set<String> sits = tableAllUser.getAllSitNums();
         for (int i = 1 ;i <= maxUsers; i++) {
              if (!sits.contains(i + "")) {
                  return i;
              }
         }
         LOG.error("no sit, please check sit");
         return 1;
    }
}
