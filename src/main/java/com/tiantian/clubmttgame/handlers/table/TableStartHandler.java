package com.tiantian.clubmttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmtt.akka.event.game.GameStartEvent;
import com.tiantian.clubmttgame.akka.event.LocalTableStartEvent;
import com.tiantian.clubmttgame.akka.event.TableTaskEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameConstants;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.constants.GameStatus;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import com.tiantian.clubmttgame.manager.model.TableUser;
import com.tiantian.clubmttgame.manager.model.UserChips;
import com.tiantian.clubmttgame.utils.GameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 */
public class TableStartHandler implements EventHandler<LocalTableStartEvent> {
    static Logger LOG = LoggerFactory.getLogger(TableStartHandler.class);
    @Override
    public void handler(LocalTableStartEvent event, ActorRef self,
                         UntypedActorContext context, ActorRef sender) {
        String tableId = event.tableId();
        List<String> userIds = event.getUserIds();
        List<GameStartEvent.UserInfo> userInfos = event.getUserInfos();
        LOG.info(tableId + ":userIds:" + JSON.toJSONString(userIds));
        LOG.info(tableId + ":userInfos:" + JSON.toJSONString(userInfos));
        if (userIds != null && userInfos != null && userIds.size() != userInfos.size()) {
            LOG.error("userIds neq userInfos");
        }

        GameStartEvent.Game game = event.getGame();
        long netxUpBlindTimes = event.getNetxUpBlindTimes();
        boolean canStart = event.isCanStart();
        TableStatus tableStatus = TableStatus.init(tableId);
        tableStatus.setTableIndex(event.getTableIndex() + "");
        tableStatus.setRoomId(game.getGameId());
        tableStatus.setGameInfo(JSON.toJSONString(game));
        tableStatus.setInningId(UUID.randomUUID().toString().replace("-", ""));
        tableStatus.setStatus(GameStatus.INIT.name());
        tableStatus.setMinUsers(6 + "");
        tableStatus.setMaxUsers(game.getTableUsers() + "");
        tableStatus.gameStop(); // 游戏未开始
        tableStatus.setLeftUsers(event.getTotalUsers() + "");
        tableStatus.setAverageChips(event.getAverageChips() + "");
        tableStatus.setMaxCanRebuyBlindLvl(game.getCanRebuyBlindLvl() + "");
        tableStatus.setReBuyInCnt(game.getBuyInCnt() + "");
        tableStatus.setCurrentBlindLevel(1 + ""); //设置盲注级别
        tableStatus.setNextUpBlindTimes(netxUpBlindTimes + "");
        tableStatus.saveNotAddCnt();

        long beginChips = game.getStartBuyIn();
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        int i = 0;
        for (GameStartEvent.UserInfo userInfo : event.getUserInfos()) {
             String sitNum = (i + 1) + "";
             TableUser tableUser = TableUser.init(userInfo.getUserId(), tableId);
             tableUser.setSitNum(sitNum);
             tableUser.setRoomId(event.getGameId());
             tableUser.setNickName(userInfo.getNickName());
             tableUser.setAvatarUrl(userInfo.getAvatarUrl());
             tableUser.setStatus(GameConstants.USER_STATUS_GAMING);
             tableUser.setGender(userInfo.getGender());
             tableUser.setTotalSecs(GameConstants.BET_DELAYER_TIME / 1000 + "");
             tableUser.setRanking("1");
             tableUser.save();
             tableAllUser.joinUser(userInfo.getUserId(), sitNum, false);
             tableAllUser = TableAllUser.load(tableId); // 重新刷新数据
             UserChips userChips = UserChips.init(userInfo.getUserId(), event.getGameId(), beginChips);
             if (event.getRebuyCnt() > 0) { //此时user只会是一个人
                 userChips.resetRebuyCnt(event.getRebuyCnt());
             }
             i++;
        }
        if (userIds != null) {
            for (String uId : userIds) {
                 TableUser tableUser = TableUser.load(uId, tableId);
                 if (tableUser == null || tableUser.isNull()) {
                     LOG.error(tableId +":tableUser is NULL,uId" + uId +",tableId:" + tableId);
                 }
                 LOG.info(tableId + ":" + JSON.toJSONString(tableUser));
            }
        }
        if (canStart) { // 直接开始
            tableStatus.gameStart(); // 游戏开始
            tableStatus.setStatus(GameStatus.INIT.name());
            tableStatus.setStartMills((System.currentTimeMillis()
                        + GameConstants.INIT_TO_BEGIN_DELAYER_TIME * 1000) + "");
            tableStatus.saveNotAddCnt();
            // 倒计时开始游戏
            JSONObject object = new JSONObject();
            object.put("inner_id", tableStatus.getInningId());
            object.put("inner_cnt", tableStatus.getIndexCount());
            object.put("game_id", tableStatus.getRoomId());
            object.put("left_secs", GameConstants.INIT_TO_BEGIN_DELAYER_TIME);
            String id = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(object, GameEventType.TABLE_BEGIN, userIds, id,  null);

            //随机一个庄位
            JSONObject object2 = new JSONObject();
            object2.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
            object2.put("tableId", tableId);
            object2.put("gameId", game.getGameId());
            object2.put("inningId", tableStatus.getInningId());
            Random random = new Random();
            TableAllUser $tableAllUser = TableAllUser.load(tableId); // 需要重新加载
            Set<String> tableSitSets = $tableAllUser.getAllSitNums();
            List<String> sitLists = new ArrayList<>(tableSitSets);
            int randomIndex = random.nextInt(sitLists.size());
            //System.out.println(randomIndex + "==randomIndex");
           // System.out.println(userIds + "==userIds");
            object2.put("randomBtn",  sitLists.get(randomIndex));
            GameUtils.pushTask(context, new TableTaskEvent(GameStatus.BEGIN.name(), object2),
                    GameConstants.INIT_TO_BEGIN_DELAYER_TIME);
        }
        else {
            //TODO 发送等待消息

        }
        sender.tell(true, ActorRef.noSender());
    }
}
