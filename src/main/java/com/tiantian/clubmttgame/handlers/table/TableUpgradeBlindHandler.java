package com.tiantian.clubmttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmttgame.akka.event.LocalTableUpgradeBlindEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.model.TableAllUser;
import com.tiantian.clubmttgame.manager.model.TableStatus;
import com.tiantian.clubmttgame.utils.GameUtils;

import java.util.UUID;

/**
 *
 */
public class TableUpgradeBlindHandler implements EventHandler<LocalTableUpgradeBlindEvent> {
    @Override
    public void handler(LocalTableUpgradeBlindEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        long ante = event.getNextAnte();
        long bigBlind = event.getNextBigBlind();
        long smallBlind = event.getNextSmallBlind();
        long upgradeTime = event.getNextUpgradeTime();
        int lvl = event.getNextLvl();
        TableAllUser tableAllUser = TableAllUser.load(event.tableId());
        TableStatus tableStatus = TableStatus.load(event.tableId());
        if (tableStatus == null) {
            return;
        }
        tableStatus.setAnte(ante + "");
        tableStatus.setSmallBlindMoney(smallBlind + "");
        tableStatus.setBigBlindMoney(bigBlind + "");
        tableStatus.setNextUpBlindTimes(upgradeTime + "");
        tableStatus.setCurrentBlindLevel(lvl + "");
        tableStatus.saveNotAddCnt();
        //发送升级盲注信息
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId() == null ? "" : tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount() == null ? "" : tableStatus.getIndexCount());
        object.put("small_blind", event.getNextSmallBlind() + "");
        object.put("big_blind", event.getNextBigBlind() + "");
        object.put("ante", event.getNextAnte() + "");
        object.put("lvl", event.getNextLvl());
        object.put("next_upgrade_secs", event.getUpgradeSecs());
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.BLIND_UP, tableAllUser.getOnlineTableUserIds(), id, tableStatus.getRoomId());
    }
}
