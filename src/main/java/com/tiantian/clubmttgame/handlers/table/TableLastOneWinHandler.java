package com.tiantian.clubmttgame.handlers.table;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.clubmttgame.akka.event.LocalGameLastOneWinEvent;
import com.tiantian.clubmttgame.handlers.EventHandler;
import com.tiantian.clubmttgame.manager.constants.GameEventType;
import com.tiantian.clubmttgame.manager.model.TableUser;
import com.tiantian.clubmttgame.utils.GameUtils;

/**
 *
 */
public class TableLastOneWinHandler implements EventHandler<LocalGameLastOneWinEvent> {
    @Override
    public void handler(LocalGameLastOneWinEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        TableUser tableUser = TableUser.load(userId, event.tableId());
        String sitNum = "1";
        if (!tableUser.isNull()) {
            sitNum = tableUser.getSitNum();
        }
        // 通知玩家离开
        JSONObject object1 = new JSONObject();
        object1.put("inner_id", "");
        object1.put("inner_cnt", "");
        object1.put("uid", userId);
        object1.put("sn", Integer.parseInt(sitNum));
        object1.put("reason", "game_over");
        // 如果不捕获异常则会终端下一个任务
        GameUtils.notifyUser(object1, GameEventType.STAND_UP, userId, event.gameId());
    }
}
