package com.tiantian.clubmttgame.server;


import com.tiantian.clubmttgame.akka.ClusterActorManager;
import com.tiantian.clubmttgame.data.mongodb.MGDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;


/**
 *
 */
public class MttGameServer {
    private static Logger LOG = LoggerFactory.getLogger(MttGameServer.class);
    public static void main(String[] args) {
        ClusterActorManager.init("5777");
        ClusterActorManager.init("5776");
        MGDatabase.getInstance().init();
        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            LOG.info("总占用内存:" + getUsedMemoryMB() + "M");
        }).start();

    }

    public static long getUsedMemoryMB()
    {
        return (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1048576; // ;
    }

}
