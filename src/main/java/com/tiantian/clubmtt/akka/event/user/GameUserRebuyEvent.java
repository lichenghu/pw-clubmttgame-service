package com.tiantian.clubmtt.akka.event.user;

import com.tiantian.clubmtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserRebuyEvent implements GameUserEvent {
    private String userId;
    private String tableId;
    private String gameId;
    private int buyInCnt;
    private String clubId;

    public GameUserRebuyEvent(String userId, String gameId, String clubId, int buyInCnt) {
        this.userId = userId;
        this.gameId = gameId;
        this.clubId = clubId;
        this.buyInCnt = buyInCnt;
    }

    @Override
    public String userId() {
        return userId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "userRebuy";
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getBuyInCnt() {
        return buyInCnt;
    }

    public void setBuyInCnt(int buyInCnt) {
        this.buyInCnt = buyInCnt;
    }

    public String getClubId() {
        return clubId;
    }

    public void setClubId(String clubId) {
        this.clubId = clubId;
    }
}
