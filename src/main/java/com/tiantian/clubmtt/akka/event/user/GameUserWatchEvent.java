package com.tiantian.clubmtt.akka.event.user;

import com.tiantian.clubmtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserWatchEvent implements GameUserEvent {
    private String gameId;
    private String userId;
    private String tableId;

    public GameUserWatchEvent(String gameId, String tableId, String userId) {
        this.gameId = gameId;
        this.userId = userId;
        this.tableId = tableId;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String event() {
        return "userWatch";
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String userId() {
        return userId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
