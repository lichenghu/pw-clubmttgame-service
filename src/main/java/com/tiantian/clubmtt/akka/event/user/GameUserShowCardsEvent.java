package com.tiantian.clubmtt.akka.event.user;

import com.tiantian.clubmtt.akka.event.GameUserEvent;

/**
 *
 */
public class GameUserShowCardsEvent implements GameUserEvent {
    private String gameId;
    private String userId;
    private String tableId;
    private String data;

    public GameUserShowCardsEvent(String gameId, String userId, String data) {
        this.gameId = gameId;
        this.userId = userId;
        this.data = data;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String event() {
        return "showCards";
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String userId() {
        return userId;
    }

    @Override
    public void setTbId(String tbId) {
        this.tableId = tbId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
