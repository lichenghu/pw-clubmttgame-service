package com.tiantian.clubmtt.akka.event;

/**
 * 玩家对游戏操作的接口
 */
public interface GameUserEvent extends GameEvent {
    String userId();
    void setTbId(String tbId);
}
