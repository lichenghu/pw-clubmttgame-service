package com.tiantian.clubmtt.akka.event.game;

import com.tiantian.clubmtt.akka.event.GameEvent;

/**
 *
 */
public class GameUserTableIdEvent implements GameEvent {
    private String gameId;
    private String userId;

    public GameUserTableIdEvent() {

    }
    public GameUserTableIdEvent(String gameId, String userId) {
        this.gameId = gameId;
        this.userId = userId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return null;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
