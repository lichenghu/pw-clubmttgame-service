package com.tiantian.clubmtt.akka.event.game;

import com.tiantian.clubmtt.akka.event.GameEvent;
import scala.Serializable;

import java.util.List;

/**
 *
 */
public class GameStartEvent implements GameEvent {
    private String gameId;
    private Game game;
    private List<String> userIds;
    private List<UserInfo> userInfos;
    public GameStartEvent(String gameId, Game game, List<String> userIds, List<UserInfo> userInfos) {
        this.gameId = gameId;
        this.game = game;
        this.userIds = userIds;
        this.userInfos = userInfos;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    @Override
    public String gameId() {
        return gameId;
    }

    @Override
    public String event() {
        return "mttStart";
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    public List<UserInfo> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }

    public static class Game implements Serializable{
        private String gameId;
        private String gameName;
        private String clubId;
        private long taxFee; //抽水费
        private long poolFee; //入池费
        private int tableUsers; // 每桌多少人
        private int minUsers; // 报名最少人数
        private int maxUsers; // 报名最多人数
        private int perRestMins; // 每多少分钟休息
        private int restMins; // 休息时长
        private int startBuyIn; //起始买入额
        private int buyInCnt; // 可以买入次数
        private int signDelayMins;// 报名延时时间分钟
        private long startMill; // 游戏开始时间
        private int canRebuyBlindLvl; // 买入最大的盲注级别 包括该级别
        private List<Reward> rewards;
        private List<Rule> rules;

        public String getGameId() {
            return gameId;
        }

        public void setGameId(String gameId) {
            this.gameId = gameId;
        }

        public long getTaxFee() {
            return taxFee;
        }

        public void setTaxFee(long taxFee) {
            this.taxFee = taxFee;
        }

        public long getPoolFee() {
            return poolFee;
        }

        public void setPoolFee(long poolFee) {
            this.poolFee = poolFee;
        }

        public int getTableUsers() {
            return tableUsers;
        }

        public void setTableUsers(int tableUsers) {
            this.tableUsers = tableUsers;
        }

        public int getMinUsers() {
            return minUsers;
        }

        public void setMinUsers(int minUsers) {
            this.minUsers = minUsers;
        }

        public int getPerRestMins() {
            return perRestMins;
        }

        public void setPerRestMins(int perRestMins) {
            this.perRestMins = perRestMins;
        }

        public int getMaxUsers() {
            return maxUsers;
        }

        public void setMaxUsers(int maxUsers) {
            this.maxUsers = maxUsers;
        }

        public int getRestMins() {
            return restMins;
        }

        public void setRestMins(int restMins) {
            this.restMins = restMins;
        }

        public int getStartBuyIn() {
            return startBuyIn;
        }

        public void setStartBuyIn(int startBuyIn) {
            this.startBuyIn = startBuyIn;
        }

        public int getBuyInCnt() {
            return buyInCnt;
        }

        public void setBuyInCnt(int buyInCnt) {
            this.buyInCnt = buyInCnt;
        }

        public int getSignDelayMins() {
            return signDelayMins;
        }

        public void setSignDelayMins(int signDelayMins) {
            this.signDelayMins = signDelayMins;
        }

        public List<Reward> getRewards() {
            return rewards;
        }

        public void setRewards(List<Reward> rewards) {
            this.rewards = rewards;
        }

        public List<Rule> getRules() {
            return rules;
        }

        public void setRules(List<Rule> rules) {
            this.rules = rules;
        }

        public long getStartMill() {
            return startMill;
        }

        public void setStartMill(long startMill) {
            this.startMill = startMill;
        }

        public int getCanRebuyBlindLvl() {
            return canRebuyBlindLvl;
        }

        public void setCanRebuyBlindLvl(int canRebuyBlindLvl) {
            this.canRebuyBlindLvl = canRebuyBlindLvl;
        }

        public String getGameName() {
            return gameName;
        }

        public void setGameName(String gameName) {
            this.gameName = gameName;
        }

        public String getClubId() {
            return clubId;
        }

        public void setClubId(String clubId) {
            this.clubId = clubId;
        }
    }

    public static class Reward implements Serializable{
        private int ranking;
        private int virtualType; // 虚拟奖励类型 1是金币
        private long virtualNums; // 虚拟奖励数据量
        private String physicalId; // 实物ID
        private String physicalName; //实物名称

        public int getRanking() {
            return ranking;
        }

        public void setRanking(int ranking) {
            this.ranking = ranking;
        }

        public int getVirtualType() {
            return virtualType;
        }

        public void setVirtualType(int virtualType) {
            this.virtualType = virtualType;
        }

        public long getVirtualNums() {
            return virtualNums;
        }

        public void setVirtualNums(long virtualNums) {
            this.virtualNums = virtualNums;
        }

        public String getPhysicalId() {
            return physicalId;
        }

        public void setPhysicalId(String physicalId) {
            this.physicalId = physicalId;
        }

        public String getPhysicalName() {
            return physicalName;
        }

        public void setPhysicalName(String physicalName) {
            this.physicalName = physicalName;
        }
    }

    public static class Rule implements Serializable{
        private int level; // 盲注等级
        private long smallBlind;
        private long bigBlind;
        private long ante; //底注
        private int upgradeSecs; // 升盲时间
        private int storeSecs; // 下注储备时间
        private int reBuyIn; // 重新买入值
        private long costMoney;

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public long getSmallBlind() {
            return smallBlind;
        }

        public void setSmallBlind(long smallBlind) {
            this.smallBlind = smallBlind;
        }

        public long getBigBlind() {
            return bigBlind;
        }

        public void setBigBlind(long bigBlind) {
            this.bigBlind = bigBlind;
        }

        public long getAnte() {
            return ante;
        }

        public void setAnte(long ante) {
            this.ante = ante;
        }

        public int getUpgradeSecs() {
            return upgradeSecs;
        }

        public void setUpgradeSecs(int upgradeSecs) {
            this.upgradeSecs = upgradeSecs;
        }

        public int getStoreSecs() {
            return storeSecs;
        }

        public void setStoreSecs(int storeSecs) {
            this.storeSecs = storeSecs;
        }

        public int getReBuyIn() {
            return reBuyIn;
        }

        public void setReBuyIn(int reBuyIn) {
            this.reBuyIn = reBuyIn;
        }

        public long getCostMoney() {
            return costMoney;
        }

        public void setCostMoney(long costMoney) {
            this.costMoney = costMoney;
        }
    }

    public static class UserInfo implements Serializable {
        private String userId;
        private String nickName;
        private String avatarUrl;
        private String gender;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }
    }
}
