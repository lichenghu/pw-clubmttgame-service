package com.tiantian.club.proxy_client;

import com.tiantian.club.settings.ClubConfig;
import com.tiantian.club.thrift.ClubService;
import com.tiantian.framework.thrift.client.ClientPool;
import com.tiantian.framework.thrift.client.IFace;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;
/**
 *
 */
public class ClubIface  extends IFace<ClubService.Iface> {

    private static class ClubIfaceHolder {
        private final static ClubIface instance = new ClubIface();
    }

    private ClubIface() {
    }

    public static ClubIface instance() {
        return ClubIfaceHolder.instance;
    }

    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new ClubService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new ClientPool(ClubConfig.getInstance().getHost(),
                ClubConfig.getInstance().getPort());
    }

    @Override
    protected Class<ClubService.Iface> faceClass() {
        return ClubService.Iface.class;
    }
}
