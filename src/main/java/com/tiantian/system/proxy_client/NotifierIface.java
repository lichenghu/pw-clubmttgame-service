package com.tiantian.system.proxy_client;

import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.system.settings.NotifierConfig;
import com.tiantian.system.thrift.notifier.NotifierService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class NotifierIface extends IFace<NotifierService.Iface> {

    private static class NotifierIfaceHolder {
        private final static NotifierIface instance = new NotifierIface();
    }

    private NotifierIface() {

    }

    public static NotifierIface instance() {
        return NotifierIfaceHolder.instance;
    }

    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new NotifierService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new com.tiantian.framework.thrift.client.ClientPool(NotifierConfig.getInstance().getHost(),
                NotifierConfig.getInstance().getPort());
    }

    @Override
    protected Class<NotifierService.Iface> faceClass() {
        return NotifierService.Iface.class;
    }
}
